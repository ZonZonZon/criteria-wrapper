CREATE TABLE my_entity
(
    id          bigserial primary key,
    name        varchar(20) NOT NULL,
    description text        NOT NULL,
    createdAt     timestamp default NOW(),
    my_sub_entity_id    bigserial,
    value int,
    money decimal(18, 2),
    stars_count bigint,
    key uuid
);

CREATE TABLE my_sub_entity
(
    id          bigserial primary key,
    name        varchar(20) NOT NULL,
    description text        NOT NULL,
    createdAt     timestamp default NOW(),
    my_sub_sub_entity_id    bigserial,
    value int,
    money decimal(18, 2),
    stars_count bigint,
    key uuid
);

CREATE TABLE my_sub_sub_entity
(
    id          bigserial primary key,
    name        varchar(20) NOT NULL,
    description text        NOT NULL,
    createdAt     timestamp default NOW(),
    value int,
    money decimal(18, 2),
    stars_count bigint,
    key uuid
);