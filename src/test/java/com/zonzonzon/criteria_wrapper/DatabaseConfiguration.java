package com.zonzonzon.criteria_wrapper;

import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.HostConfig;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Ports;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Profile;
import org.testcontainers.containers.PostgreSQLContainer;

/** See https://www.testcontainers.org/test_framework_integration/junit_5/ */
@TestConfiguration
@Profile("integration-test")
public class DatabaseConfiguration {

  public static final String USERNAME = "root";
  public static final String PASSWORD = "";
  public static final String DOCKER_IMAGE_NAME = "postgres:14.5";
  public static final int FIXED_EXTERNAL_PORT = 27000;

  static {
    // Set variables used by application-integration-test.properties file in integration tests:
    System.setProperty("DB_USERNAME", USERNAME);
    System.setProperty("DB_PASSWORD", PASSWORD);
  }

  /** Database container is specifically reconfigured to be reused by multiple tests. */
  static class DatabaseTestContainer extends PostgreSQLContainer {

    private static PostgreSQLContainer<?> container;

    private DatabaseTestContainer() {
      super(DOCKER_IMAGE_NAME);
    }

    public static PostgreSQLContainer<?> getInstance() {
      if (container == null) {
        container =
            new PostgreSQLContainer<>(DOCKER_IMAGE_NAME)
                .withDatabaseName("test")
                .withUsername("root")
                .withPassword("root")
                .withInitScript("initialize-databases.sql")
                .withReuse(true)
                .withExposedPorts(5432)
                .withCreateContainerCmdModifier(
                    cmd ->
                        cmd.withHostConfig(
                            new HostConfig()
                                .withPortBindings(
                                    new PortBinding(
                                        Ports.Binding.bindPort(FIXED_EXTERNAL_PORT),
                                        new ExposedPort(5432)))));
      }
      container.start();
      setProperties();
      return container;
    }

    @Override
    public void start() {
      super.start();
      setProperties();
    }

    private static void setProperties() {
      System.setProperty("CRITERIA_WRAPPER_USERNAME", USERNAME);
      System.setProperty("CRITERIA_WRAPPER_PASSWORD", PASSWORD);
      System.setProperty("CRITERIA_WRAPPER_HOST", container.getHost());
      System.setProperty("CRITERIA_WRAPPER_PORT", String.valueOf(FIXED_EXTERNAL_PORT));
    }

    @Override
    public void stop() {
      // Do nothing, JVM handles shut down. Provides container to be not stopped when another test
      // suite is being run.
    }
  }
}
