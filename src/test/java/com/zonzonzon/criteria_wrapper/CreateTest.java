package com.zonzonzon.criteria_wrapper;

import static org.assertj.core.api.Assertions.assertThat;

import com.zonzonzon.criteria_wrapper.domain.MyEntity;
import com.zonzonzon.criteria_wrapper.domain.MySubEntity;
import com.zonzonzon.criteria_wrapper.domain.MySubSubEntity;
import java.time.ZonedDateTime;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** A test to create data. */
class CreateTest extends BasicIT {

  private static EntityManager entityManager;

  @BeforeAll
  static void beforeAll() {
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("criteria");
    entityManager = entityManagerFactory.createEntityManager();
  }

  @BeforeEach
  void cleanUp(){
    entityManager.clear();
  }

  @Test
  @SneakyThrows
  void entityData_EntityIsPersisted_PersistedIsReadBack() {
    CriteriaRepository<MyEntity> criteriaRepository = new CriteriaRepository<>(entityManager, MyEntity.class);

    MySubSubEntity mySubSubEntity = MySubSubEntity.builder()
        .id(33)
        .name("some subsubname")
        .description("some subsubdescription")
        .createdAt(ZonedDateTime.now())
        .build();

    MySubEntity mySubEntity = MySubEntity.builder()
        .id(22)
        .name("some subname")
        .description("some subdescription")
        .createdAt(ZonedDateTime.now())
        .mySubSubEntity(mySubSubEntity)
        .build();

    MyEntity myEntity = MyEntity.builder()
        .id(1)
        .name("some name")
        .description("some description")
        .createdAt(ZonedDateTime.now())
        .mySubEntity(mySubEntity)
        .build();

    criteriaRepository.createInTransaction(myEntity);
    MyEntity result = criteriaRepository.getSingleResult();

    assertThat(result).isNotNull();
    assertThat(result.getId()).isEqualTo(myEntity.getId());
    assertThat(result.getName()).isEqualTo(myEntity.getName());
    assertThat(result.getCreatedAt()).isEqualTo(myEntity.getCreatedAt());
  }
}
