package com.zonzonzon.criteria_wrapper;

import com.zonzonzon.criteria_wrapper.DatabaseConfiguration.DatabaseTestContainer;
import org.junit.ClassRule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * A base class for other tests. Contains common settings, fields and injections. Runs Mongo
 * database and Google PubSub-emulator in docker containers. By default, Google PubSub is configured
 * with incoming test & subscription and with an outgoing test and subscription.
 */
@ContextConfiguration(classes = {DatabaseConfiguration.class})
@ActiveProfiles("integration-test")
@Testcontainers
public abstract class BasicIT {

  /**
   * Annotation @ClassRule provides that database container is not recreated on every other test
   * suite. Thus, migrations being run on the first SpringbootTest only, are available for other
   * tests too.
   */
  @ClassRule
  public static PostgreSQLContainer databaseContainer = DatabaseTestContainer.getInstance();

  @Test
  void testEnvironment_HealthChecked_FindsServers() {
    Assertions.assertTrue(databaseContainer.isRunning());
  }
}
