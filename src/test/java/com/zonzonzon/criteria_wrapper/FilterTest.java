package com.zonzonzon.criteria_wrapper;

import static org.assertj.core.api.Assertions.assertThat;

import com.zonzonzon.criteria_wrapper.domain.MyEntity;
import com.zonzonzon.criteria_wrapper.domain.MySubEntity;
import com.zonzonzon.criteria_wrapper.domain.MySubSubEntity;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.Predicate;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** A test to read data. */
class FilterTest extends BasicIT {

  private static EntityManager entityManager;
  private static ZonedDateTime now;
  private static UUID commonUuid;

  @BeforeAll
  static void beforeAll() {
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("criteria");
    entityManager = entityManagerFactory.createEntityManager();
    addInitialData();
  }

  private static void addInitialData() {
    entityManager.getTransaction().begin();
    commonUuid = UUID.randomUUID();

    now = ZonedDateTime.now();
    MySubSubEntity mySubSubEntity =
        MySubSubEntity.builder()
            .id(33)
            .name("some subsubname")
            .description("some subsubdescription")
            .createdAt(now)
            .value(33)
            .money(33.0)
            .key(commonUuid)
            .build();

    MySubEntity mySubEntity =
        MySubEntity.builder()
            .id(22)
            .name("some subname")
            .description("some subdescription")
            .createdAt(now)
            .mySubSubEntity(mySubSubEntity)
            .value(22)
            .money(22.0)
            .key(UUID.randomUUID())
            .build();

    MyEntity myEntity =
        MyEntity.builder()
            .id(1)
            .name("some name")
            .description("some description")
            .createdAt(now)
            .mySubEntity(mySubEntity)
            .value(1)
            .money(1.0)
            .starsCount(Long.MAX_VALUE - 1)
            .key(UUID.randomUUID())
            .build();

    MyEntity myEntity2 =
        MyEntity.builder()
            .id(2)
            .name("some name 2")
            .description("some description 2")
            .createdAt(now.plusDays(1L))
            .value(2)
            .money(2.0)
            .starsCount(1L)
            .key(UUID.randomUUID())
            .build();

    MyEntity myEntity3 =
        MyEntity.builder()
            .id(3)
            .name("some name 3")
            .description("some description 2")
            .createdAt(now)
            .value(2)
            .money(2.0)
            .starsCount(0L)
            .key(commonUuid)
            .build();

    MyEntity myEntity4 =
        MyEntity.builder()
            .id(4)
            .name("some name 4")
            .description("some description 4")
            .createdAt(now)
            .value(444)
            .money(444.0)
            .starsCount(444L)
            .key(UUID.randomUUID())
            .build();

    entityManager.persist(myEntity);
    entityManager.persist(myEntity2);
    entityManager.persist(myEntity3);
    entityManager.persist(myEntity4);
    entityManager.getTransaction().commit();
  }

  @BeforeEach
  void cleanUp() {
    entityManager.clear();
  }

  @Test
  @SneakyThrows
  void entities_GreaterThanAllComparedToIntegerSubEntityFields_SelectionIsReturned() {
    CriteriaRepository<MyEntity> repository =
        new CriteriaRepository<>(entityManager, MyEntity.class);

    List<MyEntity> results =
        repository.comparedToInteger(
            "value", Calculation.ALL, It.GREATER_THAN, MySubEntity.class, "value");

    assertThat(results).isNotNull().isNotEmpty();
    assertThat(results.get(0).getValue()).isNotNull().isEqualTo(444);

    // Filter out results by additional where clause:

    Predicate greaterOrEquals = repository.greaterOrEquals(555, "value");

    results =
        repository.comparedToInteger(
            "value", Calculation.ALL, It.GREATER_THAN, MySubEntity.class, "value", greaterOrEquals);

    assertThat(results).isNotNull().isEmpty();

    greaterOrEquals = repository.greaterOrEquals(333, "value");

    results =
        repository.comparedToInteger(
            "value", Calculation.ALL, It.GREATER_THAN, MySubEntity.class, "value", greaterOrEquals);

    assertThat(results).isNotNull().isNotEmpty();
    assertThat(results.get(0).getValue()).isNotNull().isEqualTo(444);
  }

  @Test
  @SneakyThrows
  void entities_EqualsAnyComparedToUuidSubEntityFields_SelectionIsReturned() {
    CriteriaRepository<MyEntity> repository =
        new CriteriaRepository<>(entityManager, MyEntity.class);

    List<MyEntity> results =
        repository.comparedToUuid("key", Calculation.ANY, It.EQUAL, MySubSubEntity.class, "key");

    assertThat(results).isNotNull().isNotEmpty();
    assertThat(results.get(0).getKey()).isNotNull().isEqualTo(commonUuid);

    // Filter out results by additional where clause:

    Predicate equals = repository.equals(UUID.randomUUID(), "key");

    results =
        repository.comparedToUuid(
            "key", Calculation.ANY, It.EQUAL, MySubSubEntity.class, "key", equals);

    assertThat(results).isNotNull().isEmpty();

    equals = repository.equals(commonUuid, "key");

    results =
        repository.comparedToUuid(
            "key", Calculation.ANY, It.EQUAL, MySubSubEntity.class, "key", equals);

    assertThat(results).isNotNull().isNotEmpty();
    assertThat(results.get(0).getKey()).isNotNull().isEqualTo(commonUuid);
  }
}
