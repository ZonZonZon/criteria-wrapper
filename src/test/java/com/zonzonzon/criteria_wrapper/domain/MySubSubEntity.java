package com.zonzonzon.criteria_wrapper.domain;

import java.time.ZonedDateTime;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MySubSubEntity {
  @Id int id;
  String name;
  String description;
  ZonedDateTime createdAt;
  Integer value;
  Double money;
  Long starsCount;
  UUID key;

  @OneToOne(mappedBy = "mySubSubEntity")
  MySubEntity mySubEntity;
}
