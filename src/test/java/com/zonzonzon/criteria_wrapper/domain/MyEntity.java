package com.zonzonzon.criteria_wrapper.domain;

import java.time.ZonedDateTime;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MyEntity {
  @Id int id;
  String name;
  String description;
  ZonedDateTime createdAt;
  Integer value;
  Double money;
  Long starsCount;
  UUID key;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "my_sub_entity_id", referencedColumnName = "id")
  MySubEntity mySubEntity;
}
