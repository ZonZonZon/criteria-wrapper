package com.zonzonzon.criteria_wrapper;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import com.zonzonzon.criteria_wrapper.domain.MyEntity;
import com.zonzonzon.criteria_wrapper.domain.MySubEntity;
import com.zonzonzon.criteria_wrapper.domain.MySubSubEntity;
import com.zonzonzon.criteria_wrapper.utils.MathUtils;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.Predicate;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** A test to read data. */
class ReadTest extends BasicIT {

  private static EntityManager entityManager;
  private static ZonedDateTime now;

  @BeforeAll
  static void beforeAll() {
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("criteria");
    entityManager = entityManagerFactory.createEntityManager();
    addInitialData();
  }

  private static void addInitialData() {
    entityManager.getTransaction().begin();

    now = ZonedDateTime.now();
    MySubSubEntity mySubSubEntity =
        MySubSubEntity.builder()
            .id(33)
            .name("some subsubname")
            .description("some subsubdescription")
            .createdAt(now)
            .value(33)
            .money(33.0)
            .build();

    MySubEntity mySubEntity =
        MySubEntity.builder()
            .id(22)
            .name("some subname")
            .description("some subdescription")
            .createdAt(now)
            .mySubSubEntity(mySubSubEntity)
            .value(22)
            .money(22.0)
            .build();

    MyEntity myEntity =
        MyEntity.builder()
            .id(1)
            .name("some name")
            .description("some description")
            .createdAt(now)
            .mySubEntity(mySubEntity)
            .value(1)
            .money(1.0)
            .starsCount(Long.MAX_VALUE - 1)
            .build();

    MyEntity myEntity2 =
        MyEntity.builder()
            .id(2)
            .name("some name 2")
            .description("some description 2")
            .createdAt(now.plusDays(1L))
            .value(2)
            .money(2.0)
            .starsCount(1L)
            .build();

    MyEntity myEntity3 =
        MyEntity.builder()
            .id(3)
            .name("some name 3")
            .description("some description 2")
            .createdAt(now)
            .value(2)
            .money(2.0)
            .starsCount(0L)
            .build();

    entityManager.persist(myEntity);
    entityManager.persist(myEntity2);
    entityManager.persist(myEntity3);
    entityManager.getTransaction().commit();
  }

  @BeforeEach
  void cleanUp() {
    entityManager.clear();
  }

  @Test
  @SneakyThrows
  void someRecords_ReadWithWhereClauseAndEmbedded_FilteredResultsWithEmbeddedAreReturned() {
    CriteriaRepository<MyEntity> repository =
        new CriteriaRepository<>(entityManager, MyEntity.class);
    Predicate whereClause = repository.inStrings(List.of("Mike", "John"), "name");
    boolean isCached = false;
    List<String> embedded = emptyList();

    List<MyEntity> results =
        repository.select().where(whereClause).getResultList(isCached, embedded);

    assertThat(results).isEmpty();

    results = repository.select().getResultList(isCached, embedded);

    assertThat(results).isNotEmpty();
    //    assertThat(results.get(0).getMySubEntity()).isNull();

    embedded = List.of("mySubEntity");

    results = repository.select().getResultList(isCached, embedded);

    assertThat(results).isNotEmpty();
    //    assertThat(results.get(0).getMySubEntity()).isNotNull();
    //    assertThat(results.get(0).getMySubEntity().getMySubSubEntity()).isNull();

    embedded = List.of("mySubEntity.mySubSubEntity");

    results = repository.select().getResultList(isCached, embedded);

    assertThat(results).isNotEmpty();
    //    assertThat(results.get(0).getMySubEntity()).isNotNull();
    //    assertThat(results.get(0).getMySubEntity().getMySubSubEntity()).isNotNull();
  }

  @Test
  @SneakyThrows
  void entitiesFields_SummedAllOrByCondition_ResultIsReturned() {
    CriteriaRepository<MyEntity> repository =
        new CriteriaRepository<>(entityManager, MyEntity.class);

    int intResult = repository.sumIntegers("value");
    assertThat(intResult).isEqualTo(5);
    intResult = repository.sumIntegers("mySubEntity.value");
    assertThat(intResult).isEqualTo(22);
    intResult = repository.sumIntegers("mySubEntity.mySubSubEntity.value");
    assertThat(intResult).isEqualTo(33);

    double doubleResult = repository.sumDoubles("money");
    assertThat(doubleResult).isEqualTo(5.0);
    doubleResult = repository.sumDoubles("mySubEntity.money");
    assertThat(doubleResult).isEqualTo(22.0);
    doubleResult = repository.sumDoubles("mySubEntity.mySubSubEntity.money");
    assertThat(doubleResult).isEqualTo(33.0);

    long longResult = repository.sumLongs("starsCount");
    assertThat(longResult).isEqualTo(Long.MAX_VALUE);

    // TODO: Implement with where clause. Currently there is a problem with different roots.
    //    Predicate whereClause = repository.greaterOrEquals(2, "value");
    //
    //    Predicate[] predicates = new Predicate[1];
    //    Criteria<MyEntity> c = repository.criteria;
    //    predicates[0] = c.getBuilder().ge(c.getRoot().get("value"), 2);
    //    c.getReadQuery().select(c.getRoot()).where(predicates);
    //    List<MyEntity> resultList = entityManager.createQuery(c.getReadQuery()).getResultList();
    //
    //    CriteriaQuery<Number> query = c.getBuilder().createQuery(Number.class);
    //    Root<MyEntity> root = query.from(MyEntity.class);
    //    Expression<Number> fieldPath = c.getPath("value", root);
    //    Expression<Number> sum = c.getBuilder().sum(fieldPath);
    //    CriteriaQuery<Number> sumQuery = query.select(sum.alias("D")).where(predicates);
    //    Number singleResult = entityManager.createQuery(sumQuery).getSingleResult();
    //    int intResult = repository.sumIntegers("value", predicates);
    //    assertThat(resultList).isEqualTo(2);
  }

  @Test
  @SneakyThrows
  void entitiesFields_AverageOrByCondition_ResultIsReturned() {
    CriteriaRepository<MyEntity> repository =
        new CriteriaRepository<>(entityManager, MyEntity.class);

    double doubleResult = repository.average("money");
    assertThat(MathUtils.round(doubleResult, 2)).isEqualTo(1.67);
    doubleResult = repository.sumDoubles("mySubEntity.money");
    assertThat(doubleResult).isEqualTo(22.0);
    doubleResult = repository.sumDoubles("mySubEntity.mySubSubEntity.money");
    assertThat(doubleResult).isEqualTo(33.0);

    // TODO: Implement with where clause. Currently there is a problem with different roots.
  }

  @Test
  @SneakyThrows
  void entitiesFields_MaxMinOrByCondition_ResultIsReturned() {
    CriteriaRepository<MyEntity> repository =
        new CriteriaRepository<>(entityManager, MyEntity.class);

    int intResult = repository.maxInteger("value");
    assertThat(intResult).isEqualTo(2);
    intResult = repository.maxInteger("mySubEntity.value");
    assertThat(intResult).isEqualTo(22);
    intResult = repository.maxInteger("mySubEntity.mySubSubEntity.value");
    assertThat(intResult).isEqualTo(33);

    double doubleResult = repository.maxDouble("money");
    assertThat(doubleResult).isEqualTo(2.0);
    doubleResult = repository.maxDouble("mySubEntity.money");
    assertThat(doubleResult).isEqualTo(22.0);
    doubleResult = repository.maxDouble("mySubEntity.mySubSubEntity.money");
    assertThat(doubleResult).isEqualTo(33.0);

    long longResult = repository.maxLong("starsCount");
    assertThat(longResult).isEqualTo(Long.MAX_VALUE - 1);

    ZonedDateTime dateTimeResult = repository.maxDateTime("createdAt");
    assertThat(dateTimeResult).isEqualTo(now.plusDays(1L));
    dateTimeResult = repository.maxDateTime("mySubEntity.createdAt");
    assertThat(dateTimeResult).isEqualTo(now);
    dateTimeResult = repository.maxDateTime("mySubEntity.mySubSubEntity.createdAt");
    assertThat(dateTimeResult).isEqualTo(now);

    intResult = repository.minInteger("value");
    assertThat(intResult).isEqualTo(1);
    intResult = repository.minInteger("mySubEntity.value");
    assertThat(intResult).isEqualTo(22);
    intResult = repository.minInteger("mySubEntity.mySubSubEntity.value");
    assertThat(intResult).isEqualTo(33);

    doubleResult = repository.minDouble("money");
    assertThat(doubleResult).isEqualTo(1.0);
    doubleResult = repository.minDouble("mySubEntity.money");
    assertThat(doubleResult).isEqualTo(22.0);
    doubleResult = repository.minDouble("mySubEntity.mySubSubEntity.money");
    assertThat(doubleResult).isEqualTo(33.0);

    longResult = repository.minLong("starsCount");
    assertThat(longResult).isZero();

    dateTimeResult = repository.minDateTime("createdAt");
    assertThat(dateTimeResult).isEqualTo(now);
    dateTimeResult = repository.minDateTime("mySubEntity.createdAt");
    assertThat(dateTimeResult).isEqualTo(now);
    dateTimeResult = repository.minDateTime("mySubEntity.mySubSubEntity.createdAt");
    assertThat(dateTimeResult).isEqualTo(now);

    // TODO: Implement with where clause. Currently there is a problem with different roots.
  }

  @Test
  @SneakyThrows
  void entitiesFields_CountedAllOrDistinctOrByCondition_ResultIsReturned() {
    CriteriaRepository<MyEntity> repository =
        new CriteriaRepository<>(entityManager, MyEntity.class);

    long longResult = repository.count("value");
    assertThat(longResult).isEqualTo(3);
    longResult = repository.count("mySubEntity.value");
    assertThat(longResult).isEqualTo(1);
    longResult = repository.count("mySubEntity.mySubSubEntity.value");
    assertThat(longResult).isEqualTo(1);

    longResult = repository.count("money");
    assertThat(longResult).isEqualTo(3);
    longResult = repository.count("mySubEntity.money");
    assertThat(longResult).isEqualTo(1);
    longResult = repository.count("mySubEntity.mySubSubEntity.money");
    assertThat(longResult).isEqualTo(1);

    longResult = repository.count("starsCount");
    assertThat(longResult).isEqualTo(3);
    longResult = repository.count("mySubEntity.starsCount");
    assertThat(longResult).isZero();
    longResult = repository.count("mySubEntity.mySubSubEntity.starsCount");
    assertThat(longResult).isZero();

    longResult = repository.count("createdAt");
    assertThat(longResult).isEqualTo(3);
    longResult = repository.count("mySubEntity.createdAt");
    assertThat(longResult).isEqualTo(1);
    longResult = repository.count("mySubEntity.mySubSubEntity.createdAt");
    assertThat(longResult).isEqualTo(1);

    longResult = repository.count("description");
    assertThat(longResult).isEqualTo(3);
    longResult = repository.count("mySubEntity.description");
    assertThat(longResult).isEqualTo(1);
    longResult = repository.count("mySubEntity.mySubSubEntity.description");
    assertThat(longResult).isEqualTo(1);

    longResult = repository.countDistinct("value");
    assertThat(longResult).isEqualTo(2);
    longResult = repository.countDistinct("mySubEntity.value");
    assertThat(longResult).isEqualTo(1);
    longResult = repository.countDistinct("mySubEntity.mySubSubEntity.value");
    assertThat(longResult).isEqualTo(1);

    longResult = repository.countDistinct("money");
    assertThat(longResult).isEqualTo(2);
    longResult = repository.countDistinct("mySubEntity.money");
    assertThat(longResult).isEqualTo(1);
    longResult = repository.countDistinct("mySubEntity.mySubSubEntity.money");
    assertThat(longResult).isEqualTo(1);

    longResult = repository.countDistinct("starsCount");
    assertThat(longResult).isEqualTo(3);
    longResult = repository.countDistinct("mySubEntity.starsCount");
    assertThat(longResult).isZero();
    longResult = repository.countDistinct("mySubEntity.mySubSubEntity.starsCount");
    assertThat(longResult).isZero();

    longResult = repository.countDistinct("createdAt");
    assertThat(longResult).isEqualTo(2);
    longResult = repository.countDistinct("mySubEntity.createdAt");
    assertThat(longResult).isEqualTo(1);
    longResult = repository.countDistinct("mySubEntity.mySubSubEntity.createdAt");
    assertThat(longResult).isEqualTo(1);

    longResult = repository.countDistinct("description");
    assertThat(longResult).isEqualTo(2);
    longResult = repository.countDistinct("mySubEntity.description");
    assertThat(longResult).isEqualTo(1);
    longResult = repository.countDistinct("mySubEntity.mySubSubEntity.description");
    assertThat(longResult).isEqualTo(1);

    // TODO: Implement with where clause. Currently there is a problem with different roots.
  }
}
