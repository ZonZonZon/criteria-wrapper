package com.zonzonzon.criteria_wrapper;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import com.zonzonzon.criteria_wrapper.domain.MyEntity;
import com.zonzonzon.criteria_wrapper.domain.MySubEntity;
import com.zonzonzon.criteria_wrapper.domain.MySubSubEntity;
import java.time.ZonedDateTime;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** A test to update data. */
class UpdateTest extends BasicIT {

  private static EntityManager entityManager;

  @BeforeAll
  static void beforeAll() {
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("criteria");
    entityManager = entityManagerFactory.createEntityManager();
    addInitialData();
  }

  private static void addInitialData() {
    entityManager.getTransaction().begin();

    MySubSubEntity mySubSubEntity =
        MySubSubEntity.builder()
            .id(33)
            .name("some subsubname")
            .description("some subsubdescription")
            .createdAt(ZonedDateTime.now())
            .value(33)
            .money(111D)
            .starsCount(0L)
            .build();

    MySubEntity mySubEntity =
        MySubEntity.builder()
            .id(22)
            .name("some subname")
            .description("some subdescription")
            .createdAt(ZonedDateTime.now())
            .mySubSubEntity(mySubSubEntity)
            .value(22)
            .money(111D)
            .starsCount(0L)
            .build();

    MyEntity myEntity =
        MyEntity.builder()
            .id(1)
            .name("some name")
            .description("some description")
            .createdAt(ZonedDateTime.now())
            .mySubEntity(mySubEntity)
            .value(1)
            .money(111D)
            .starsCount(20L)
            .build();

    MyEntity myEntity2 =
        MyEntity.builder()
            .id(2)
            .name("some name 2")
            .description("some description 2")
            .createdAt(ZonedDateTime.now())
            .value(2)
            .money(111D)
            .starsCount(10L)
            .build();

    entityManager.persist(myEntity);
    entityManager.persist(myEntity2);
    entityManager.getTransaction().commit();
  }

  @BeforeEach
  void cleanUp(){
    entityManager.clear();
  }

  @Test
  @SneakyThrows
  void entity_DataIsUpdated_UpdatedEntityIsReturned() {
    CriteriaRepository<MyEntity> repository =
        new CriteriaRepository<>(entityManager, MyEntity.class);

    MyEntity myEntity =
        repository.selectAll().getResultList().stream()
            .filter(myEntity1 -> myEntity1.getId() == 1)
            .findAny()
            .orElse(null);

    myEntity.setName("new name");
    repository.updateInTransaction(myEntity);

    MyEntity result =
        repository.selectAll().getResultList().stream()
            .filter(myEntity1 -> myEntity1.getId() == 1)
            .findAny()
            .orElse(null);

    assertThat(result).isNotNull();
    assertThat(result.getId()).isEqualTo(myEntity.getId());
    assertThat(result.getName()).isEqualTo("new name");
    assertThat(result.getCreatedAt()).isEqualTo(myEntity.getCreatedAt());
  }

  @Test
  @SneakyThrows
  void entityType_EntitiesFiltered_AreIncrementedByAGivenValue() {
    CriteriaRepository<MyEntity> repository = new CriteriaRepository<>(entityManager, MyEntity.class);

    repository.runInTransaction(
        parameters -> repository.increment("value", 10), emptyList());

    int intSum = repository.selectAll().getResultList().stream().mapToInt(MyEntity::getValue).sum();
    assertThat(intSum).isEqualTo(23);

    repository.runInTransaction(
        parameters -> repository.increment("starsCount", 10L), emptyList());

    long longSum = repository.selectAll().getResultList().stream().mapToLong(MyEntity::getStarsCount).sum();
    assertThat(longSum).isEqualTo(30L);

    repository.runInTransaction(
        parameters -> repository.increment("money", 111D), emptyList());

    double doubleSum = repository.selectAll().getResultList().stream().mapToDouble(MyEntity::getMoney).sum();
    assertThat(doubleSum).isEqualTo(222D);
  }
}
