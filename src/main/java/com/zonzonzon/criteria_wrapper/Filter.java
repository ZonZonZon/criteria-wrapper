package com.zonzonzon.criteria_wrapper;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.persistence.metamodel.SingularAttribute;

/**
 * Methods providing filtering conditions for different clauses.
 *
 * @param <T>
 */
public interface Filter<T> extends Common<T> {

  /**
   * SQL-like expression for IN of WHERE clauses. Searches for embedded entity fields. Currently, is
   * implemented up to the 2nd level of propagation.
   */
  default Predicate in(Collection<?> list, SingularAttribute<T, ?>... entityFields) {
    // TODO: Add propagation levels.
    Path<?> path = getPath(entityFields);
    Predicate predicate = is(list, It.CONTAIN, path);
    return predicate;
  }

  /**
   * Overloaded method. См. {@link Criteria#in(Collection, SingularAttribute[])}
   *
   * @param type Optional. Is considered {@link It#CONTAIN}.
   */
  default Predicate is(List<?> list, It type, SingularAttribute<T, ?>... entityField) {
    return in(list, entityField);
  }

  /** См. {@link Filter#is(Object, It, Path)} */
  default <Y> Predicate is(Object expected, It type, Path<Y> given) {
    Predicate predicate = null;
    if (expected == null || (expected instanceof List && ((List) expected).size() == 0)) {
      // No parameter:
      return getSkipped();
    }
    if (expected instanceof LocalDate) {
      return isForDate(type, (Path<LocalDate>) given, (LocalDate) expected);
    }
    if (expected instanceof Integer) {
      return isForInteger(type, (Path<Integer>) given, (Integer) expected);
    }
    if (expected instanceof Long) {
      return isForLong(type, (Path<Long>) given, (Long) expected);
    }
    if (expected instanceof Double) {
      return isForDouble(type, (Path<Double>) given, (Double) expected);
    }
    // Other values:
    switch (type) {
      case EQUAL:
        {
          predicate = getBuilder().equal(given, expected);
          break;
        }
      case NOT_EQUAL:
      {
        predicate = getBuilder().notEqual(given, expected);
        break;
      }
      case IN:
      case CONTAIN:
        {
          try {
            //noinspection ConstantConditions,unchecked
            predicate = given.in((Collection<?>) expected);

          } catch (ClassCastException ignore) {
          }
          break;
        }
      default:
        {
          break;
        }
    }

    return predicate;
  }

  /**
   * Return empty positive query predicate if NULL. Then condition is skipped and doesn't influence
   * other predicates.
   *
   * <p>Allows to create overloaded methods to ignore certain filter parameters. Allows comparing
   * entities, even if NULL.
   *
   * @param expected Val ue to search for.
   * @param entityField Field of entity.
   * @return Query conditions predicate.
   */
  default <Y> Predicate is(Object expected, It type, SingularAttribute<T, ?>... entityField) {
    Path<Y> given = getPath(entityField);
    return is(expected, type, given);
  }

  /**
   * Special method for dates. Abstract method cannot be used for generics of CriteriaBuilder below.
   * See {@link Criteria#is(Object, It, Path)}
   */
  default Predicate isForDate(It type, Path<LocalDate> given, LocalDate expected) {
    Predicate predicate = null;
    switch (type) {
      case GREATER_THAN:
      {
        predicate = getBuilder().greaterThan(given, expected);
        break;
      }
      case LESS_THAN:
      {
        predicate = getBuilder().lessThan(given, expected);
        break;
      }
      case GREATER_OR_EQUAL:
      {
        predicate = getBuilder().greaterThanOrEqualTo(given, expected);
        break;
      }
      case LESS_OR_EQUAL:
      {
        predicate = getBuilder().lessThanOrEqualTo(given, expected);
        break;
      }
      case EQUAL:
      {
        predicate = getBuilder().equal(given, expected);
        break;
      }
      case NOT_EQUAL:
      {
        predicate = getBuilder().notEqual(given, expected);
        break;
      }
      default:
      {
        break;
      }
    }
    return predicate;
  }

  /**
   * Special method for Integers. Abstract method cannot be used for generics of CriteriaBuilder below.
   * See {@link Criteria#is(Object, It, Path)}
   */
  default Predicate isForInteger(It type, Path<Integer> given, Integer expected) {
    Predicate predicate = null;
    switch (type) {
      case GREATER_THAN:
      {
        predicate = getBuilder().greaterThan(given, expected);
        break;
      }
      case LESS_THAN:
      {
        predicate = getBuilder().lessThan(given, expected);
        break;
      }
      case GREATER_OR_EQUAL:
      {
        predicate = getBuilder().greaterThanOrEqualTo(given, expected);
        break;
      }
      case LESS_OR_EQUAL:
      {
        predicate = getBuilder().lessThanOrEqualTo(given, expected);
        break;
      }
      case EQUAL:
      {
        predicate = getBuilder().equal(given, expected);
        break;
      }
      case NOT_EQUAL:
      {
        predicate = getBuilder().notEqual(given, expected);
        break;
      }
      default:
      {
        break;
      }
    }
    return predicate;
  }

  /**
   * Special method for Longs. Abstract method cannot be used for generics of CriteriaBuilder below.
   * See {@link Criteria#is(Object, It, Path)}
   */
  default Predicate isForLong(It type, Path<Long> given, Long expected) {
    Predicate predicate = null;
    switch (type) {
      case GREATER_THAN:
      {
        predicate = getBuilder().greaterThan(given, expected);
        break;
      }
      case LESS_THAN:
      {
        predicate = getBuilder().lessThan(given, expected);
        break;
      }
      case GREATER_OR_EQUAL:
      {
        predicate = getBuilder().greaterThanOrEqualTo(given, expected);
        break;
      }
      case LESS_OR_EQUAL:
      {
        predicate = getBuilder().lessThanOrEqualTo(given, expected);
        break;
      }
      case EQUAL:
      {
        predicate = getBuilder().equal(given, expected);
        break;
      }
      case NOT_EQUAL:
      {
        predicate = getBuilder().notEqual(given, expected);
        break;
      }
      default:
      {
        break;
      }
    }
    return predicate;
  }

  /**
   * Special method for Doubles. Abstract method cannot be used for generics of CriteriaBuilder below.
   * See {@link Criteria#is(Object, It, Path)}
   */
  default Predicate isForDouble(It type, Path<Double> given, Double expected) {
    Predicate predicate = null;
    switch (type) {
      case GREATER_THAN:
      {
        predicate = getBuilder().greaterThan(given, expected);
        break;
      }
      case LESS_THAN:
      {
        predicate = getBuilder().lessThan(given, expected);
        break;
      }
      case GREATER_OR_EQUAL:
      {
        predicate = getBuilder().greaterThanOrEqualTo(given, expected);
        break;
      }
      case LESS_OR_EQUAL:
      {
        predicate = getBuilder().lessThanOrEqualTo(given, expected);
        break;
      }
      case EQUAL:
      {
        predicate = getBuilder().equal(given, expected);
        break;
      }
      case NOT_EQUAL:
      {
        predicate = getBuilder().notEqual(given, expected);
        break;
      }
      default:
      {
        break;
      }
    }
    return predicate;
  }

  default Predicate getSkipped() {
    return getBuilder().conjunction();
  }

  /**
   * SQL-like expression IN for WHERE.
   *
   * @param inList A list for IN.
   * @param expression PAth to entity field.
   * @return A ready predicate to be inserted into WHERE.
   */
  default Predicate inCommon(List<?> inList, Expression<?> expression) {
    Predicate predicate = expression.in(inList);
    return predicate;
  }

  /**
   * In for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate inUuids(Collection<UUID> value, String nestedObjectValue) {
    return inCommon(
        value == null ? emptyList() : asList(value.toArray()), getPath(nestedObjectValue));
  }

  default Predicate inUuids(Collection<UUID> value, SingularAttribute<T, Object> entityField) {
    return inCommon(
        value == null ? emptyList() : asList(value.toArray()), getRoot().get(entityField));
  }

  /**
   * In for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate inObjects(List<?> value, String nestedObjectValue) {
    return inCommon(value, getPath(nestedObjectValue));
  }

  default Predicate inObjects(List<?> value, SingularAttribute<T, ?> entityField) {
    return inCommon(value, getRoot().get(entityField));
  }

  default Predicate inObjects(List<?> value, Path<?> entityField) {
    return inCommon(value, entityField);
  }

  /**
   * In for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate inIntegers(List<Integer> value, String nestedObjectValue) {
    return inCommon(value, getPath(nestedObjectValue));
  }

  default Predicate inIntegers(List<Integer> value, SingularAttribute<T, Integer> entityField) {
    return inCommon(value, getRoot().get(entityField));
  }

  /**
   * In for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate inStrings(List<String> value, String nestedObjectValue) {
    return inCommon(value, getPath(nestedObjectValue));
  }

  default Predicate inStrings(List<?> value, SingularAttribute<T, String> entityField) {
    return inCommon(value, getRoot().get(entityField));
  }

  /**
   * In for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate inLocalDates(List<LocalDate> value, String nestedObjectValue) {
    return inCommon(value, getPath(nestedObjectValue));
  }

  default Predicate inLocalDates(List<?> value, SingularAttribute<T, LocalDate> entityField) {
    return inCommon(value, getRoot().get(entityField));
  }

  /**
   * In for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate inBooleans(List<Boolean> value, String nestedObjectValue) {
    return inCommon(value, getPath(nestedObjectValue));
  }

  default Predicate inBooleans(List<?> value, SingularAttribute<T, Boolean> entityField) {
    return inCommon(value, getRoot().get(entityField));
  }

  /**
   * In for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate inDoubles(List<Boolean> value, String nestedObjectValue) {
    return inCommon(value, getPath(nestedObjectValue));
  }

  default Predicate inDoubles(List<?> value, SingularAttribute<T, Double> entityField) {
    return inCommon(value, getRoot().get(entityField));
  }

  /**
   * SQL-like expression "=" for WHERE and JOIN fields. Searches fort the value in an embedded
   * entity. Is currently implemented for two-levels of propagation.
   */
  default Predicate equals(Integer value, SingularAttribute<T, ?>... entityFields) {
    Path<Integer> path = getPath(entityFields);
    Predicate predicate = equals(value, path);
    return predicate;
  }

  /**
   * Equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default <F> Predicate equals(F value, String nestedObjectValue) {
    return equalsCommon(value, getPath(nestedObjectValue));
  }

  /**
   * SQL-like expression "=" and IS NULL for WHERE.
   *
   * @param value Value to compare.
   * @param expression Path to entity field.
   * @return A ready to insert predicate for WHERE.
   */
  default <F> Predicate equalsCommon(F value, Expression<?> expression) {
    Predicate predicate;
    if (value == null) {
      predicate = getBuilder().isNull(expression);
    } else {
      predicate = getBuilder().equal(expression, value);
    }
    return predicate;
  }

  default <F> Predicate equals(Object value, SingularAttribute<T, F> entityField) {
    return equalsCommon(value, getRoot().get(entityField));
  }

  /**
   * Equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate equals(UUID value, String nestedObjectValue) {
    return equalsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate equals(UUID value, SingularAttribute<T, UUID> entityField) {
    return equalsCommon(value, getRoot().get(entityField));
  }

  /**
   * Equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate equals(LocalDate value, String nestedObjectValue) {
    return equalsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate equals(LocalDate value, SingularAttribute<T, LocalDate> entityField) {
    return equalsCommon(value, getRoot().get(entityField));
  }

  /**
   * Equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate equals(Integer value, String nestedObjectValue) {
    return equalsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate equals(Integer value, SingularAttribute<T, Integer> entityField) {
    return equalsCommon(value, getRoot().get(entityField));
  }

  default Predicate equals(Integer value, Path path) {
    return equalsCommon(value, path);
  }

  /**
   * Equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate equals(String value, String nestedObjectValue) {
    return equalsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate equals(String value, SingularAttribute<T, String> entityField) {
    return equalsCommon(value, getRoot().get(entityField));
  }

  /**
   * Equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate equals(Boolean value, String nestedObjectValue) {
    return equalsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate equals(Boolean value, SingularAttribute<T, Boolean> entityField) {
    return equalsCommon(value, getRoot().get(entityField));
  }

  /**
   * Equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate equals(Double value, String nestedObjectValue) {
    return equalsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate equals(Double value, SingularAttribute<T, Double> entityField) {
    return equalsCommon(value, getRoot().get(entityField));
  }

  /**
   * Greater or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default <F> Predicate greaterOrEquals(F value, String nestedObjectValue) {
    return greaterOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  /**
   * SQL-like expression ">=" for WHERE.
   *
   * @param value Value to compare.
   * @param path Path to entity field.
   * @return A ready to insert predicate for WHERE.
   */
  default Predicate greaterOrEqualsCommon(Object value, Path path) {
    Predicate predicate;
    predicate = is(value, It.GREATER_OR_EQUAL, path);
    return predicate;
  }

  default <F> Predicate greaterOrEquals(F value, SingularAttribute<T, F> entityField) {
    return greaterOrEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Greater or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate greaterOrEquals(LocalDate value, String nestedObjectValue) {
    return greaterOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate greaterOrEquals(LocalDate value, SingularAttribute<T, LocalDate> entityField) {
    return greaterOrEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Greater or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate greaterOrEquals(Integer value, String nestedObjectValue) {
    return greaterOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate greaterOrEquals(Integer value, SingularAttribute<T, Integer> entityField) {
    return greaterOrEqualsCommon(value, getRoot().get(entityField));
  }

  default Predicate greaterOrEquals(Integer value, Path path) {
    return greaterOrEqualsCommon(value, path);
  }

  /**
   * Greater or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate greaterOrEquals(String value, String nestedObjectValue) {
    return equalsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate greaterOrEquals(String value, SingularAttribute<T, String> entityField) {
    return greaterOrEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Greater or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate greaterOrEquals(Boolean value, String nestedObjectValue) {
    return greaterOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate greaterOrEquals(Boolean value, SingularAttribute<T, Boolean> entityField) {
    return greaterOrEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Greater or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate greaterOrEquals(Double value, String nestedObjectValue) {
    return greaterOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate greaterOrEquals(Double value, SingularAttribute<T, Double> entityField) {
    return greaterOrEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * SQL-like "<=" for WHERE.
   *
   * @param value Value to compare.
   * @param path Path to entity field.
   * @return A ready to insert predicate for WHERE.
   */
  default Predicate lessOrEqualsCommon(Object value, Path path) {
    Predicate predicate;
    predicate = is(value, It.LESS_OR_EQUAL, path);
    return predicate;
  }

  /**
   * Less or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default <F> Predicate lessOrEquals(F value, String nestedObjectValue) {
    return lessOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  default <F> Predicate lessOrEquals(F value, SingularAttribute<T, F> entityField) {
    return lessOrEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Less or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate lessOrEquals(LocalDate value, String nestedObjectValue) {
    return lessOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate lessOrEquals(LocalDate value, SingularAttribute<T, LocalDate> entityField) {
    return lessOrEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Less or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate lessOrEquals(Integer value, String nestedObjectValue) {
    return lessOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate lessOrEquals(Integer value, SingularAttribute<T, Integer> entityField) {
    return lessOrEqualsCommon(value, getRoot().get(entityField));
  }

  default Predicate lessOrEquals(Integer value, Path path) {
    return lessOrEqualsCommon(value, path);
  }

  /**
   * Less or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate lessOrEquals(String value, String nestedObjectValue) {
    return lessOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate lessOrEquals(String value, SingularAttribute<T, String> entityField) {
    return lessOrEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Less or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate lessOrEquals(Boolean value, String nestedObjectValue) {
    return lessOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate lessOrEquals(Boolean value, SingularAttribute<T, Boolean> entityField) {
    return lessOrEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Less or equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate lessOrEquals(Double value, String nestedObjectValue) {
    return lessOrEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate lessOrEquals(Double value, SingularAttribute<T, Double> entityField) {
    return lessOrEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * SQL-like expression "!=" and IS NOT NULL for WHERE.
   *
   * @param value Value to compare.
   * @param expression A ready expression with a known type.
   * @return A ready to insert predicate for WHERE.
   */
  default Predicate notEqualsCommon(Object value, Expression<?> expression) {
    Predicate predicate;
    if (value == null) {
      predicate = getBuilder().isNotNull(expression);
    } else {
      predicate = getBuilder().notEqual(expression, value);
    }
    return predicate;
  }

  /**
   * Not equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default <F> Predicate notEquals(F value, String nestedObjectValue) {
    return notEqualsCommon(value, getPath(nestedObjectValue));
  }

  default <F> Predicate notEquals(F value, SingularAttribute<T, F> entityField) {
    return notEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Not equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate notEquals(Integer value, String nestedObjectValue) {
    return notEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate notEquals(Integer value, SingularAttribute<T, Integer> entityField) {
    return notEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Not equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate notEquals(String value, String nestedObjectValue) {
    return notEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate notEquals(String value, SingularAttribute<T, String> entityField) {
    return notEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Not equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate notEquals(Boolean value, String nestedObjectValue) {
    return notEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate notEquals(Boolean value, SingularAttribute<T, Boolean> entityField) {
    return notEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Not equals for nested object value.
   *
   * @param value Value for comparison.
   * @param nestedObjectValue Dot separator in string is interpreted as nesting.
   */
  default Predicate notEquals(Double value, String nestedObjectValue) {
    return notEqualsCommon(value, getPath(nestedObjectValue));
  }

  default Predicate notEquals(Double value, SingularAttribute<T, Double> entityField) {
    return notEqualsCommon(value, getRoot().get(entityField));
  }

  /**
   * Prepares and executes a query to get Integer results when one field is compared to others.
   *
   * @param leftEntityFieldPath A dot-chained path to a field.
   * @param rightEntityFieldPath A dot-chained path to a field.
   * @param conditions Query conditions to filter the selection.
   * @return A query single result - result Integer.
   */
  default <R> List<T> getIntegerComparedResults(
      String leftEntityFieldPath,
      Calculation calculation,
      It compared,
      Class<R> rightEntityType,
      String rightEntityFieldPath,
      Predicate... conditions) {

    Criteria<R> subEntityCriteria = new Criteria<>(getEntityManager(), rightEntityType);
    Subquery<Integer> subquery = getReadQuery().subquery(Integer.class);
    Root<R> subRoot = subquery.from(rightEntityType);
    Path<Integer> leftFieldPath = getPath(leftEntityFieldPath, getRoot());
    Path<Integer> rightFieldPath = subEntityCriteria.getPath(rightEntityFieldPath, subRoot);
    subquery.select(rightFieldPath);

    Predicate[] combinedConditions = null;
    switch (calculation) {
      case ALL:
        if (It.GREATER_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThan(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.LESS_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThan(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.GREATER_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThanOrEqualTo(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.LESS_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThanOrEqualTo(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().equal(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.NOT_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().notEqual(leftFieldPath, getBuilder().all(subquery));
        }
        break;
      case ANY:
        if (It.GREATER_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThan(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.LESS_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThan(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.GREATER_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThanOrEqualTo(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.LESS_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThanOrEqualTo(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().equal(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.NOT_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().notEqual(leftFieldPath, getBuilder().any(subquery));
        }
        break;
      default:
        break;
    }

    if (combinedConditions != null) {
      getReadQuery().select(getRoot()).where(combinedConditions);
    } else {
      getReadQuery().select(getRoot());
    }

    final TypedQuery<?> typedQuery = getEntityManager().createQuery(getReadQuery());
    List<T> results = (List<T>) typedQuery.getResultList();
    return results;
  }

  /**
   * Prepares and executes a query to get Long results when one field is compared to others.
   *
   * @param leftEntityFieldPath A dot-chained path to a field.
   * @param rightEntityFieldPath A dot-chained path to a field.
   * @param conditions Query conditions to filter the selection.
   * @return A query single result - result Long.
   */
  default <R> List<T> getLongComparedResults(
      String leftEntityFieldPath,
      Calculation calculation,
      It compared,
      Class<R> rightEntityType,
      String rightEntityFieldPath,
      Predicate... conditions) {

    Criteria<R> subEntityCriteria = new Criteria<>(getEntityManager(), rightEntityType);
    Subquery<Long> subquery = getReadQuery().subquery(Long.class);
    Root<R> subRoot = subquery.from(rightEntityType);
    Path<Long> leftFieldPath = getPath(leftEntityFieldPath, getRoot());
    Path<Long> rightFieldPath = subEntityCriteria.getPath(rightEntityFieldPath, subRoot);
    subquery.select(rightFieldPath);

    Predicate[] combinedConditions = null;
    switch (calculation) {
      case ALL:
        if (It.GREATER_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThan(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.LESS_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThan(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.GREATER_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThanOrEqualTo(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.LESS_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThanOrEqualTo(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().equal(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.NOT_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().notEqual(leftFieldPath, getBuilder().all(subquery));
        }
        break;
      case ANY:
        if (It.GREATER_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThan(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.LESS_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThan(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.GREATER_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThanOrEqualTo(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.LESS_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThanOrEqualTo(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().equal(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.NOT_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().notEqual(leftFieldPath, getBuilder().any(subquery));
        }
        break;
      default:
        break;
    }

    if (combinedConditions != null) {
      getReadQuery().select(getRoot()).where(combinedConditions);
    } else {
      getReadQuery().select(getRoot());
    }

    final TypedQuery<?> typedQuery = getEntityManager().createQuery(getReadQuery());
    List<T> results = (List<T>) typedQuery.getResultList();
    return results;
  }

  /**
   * Prepares and executes a query to get Double results when one field is compared to others.
   *
   * @param leftEntityFieldPath A dot-chained path to a field.
   * @param rightEntityFieldPath A dot-chained path to a field.
   * @param conditions Query conditions to filter the selection.
   * @return A query single result - result Double.
   */
  default <R> List<T> getDoubleComparedResults(
      String leftEntityFieldPath,
      Calculation calculation,
      It compared,
      Class<R> rightEntityType,
      String rightEntityFieldPath,
      Predicate... conditions) {

    Criteria<R> subEntityCriteria = new Criteria<>(getEntityManager(), rightEntityType);
    Subquery<Double> subquery = getReadQuery().subquery(Double.class);
    Root<R> subRoot = subquery.from(rightEntityType);
    Path<Double> leftFieldPath = getPath(leftEntityFieldPath, getRoot());
    Path<Double> rightFieldPath = subEntityCriteria.getPath(rightEntityFieldPath, subRoot);
    subquery.select(rightFieldPath);

    Predicate[] combinedConditions = null;
    switch (calculation) {
      case ALL:
        if (It.GREATER_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThan(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.LESS_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThan(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.GREATER_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThanOrEqualTo(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.LESS_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThanOrEqualTo(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().equal(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.NOT_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().notEqual(leftFieldPath, getBuilder().all(subquery));
        }
        break;
      case ANY:
        if (It.GREATER_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThan(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.LESS_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThan(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.GREATER_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThanOrEqualTo(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.LESS_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThanOrEqualTo(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().equal(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.NOT_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().notEqual(leftFieldPath, getBuilder().any(subquery));
        }
        break;
      default:
        break;
    }

    if (combinedConditions != null) {
      getReadQuery().select(getRoot()).where(combinedConditions);
    } else {
      getReadQuery().select(getRoot());
    }

    final TypedQuery<?> typedQuery = getEntityManager().createQuery(getReadQuery());
    List<T> results = (List<T>) typedQuery.getResultList();
    return results;
  }

  /**
   * Prepares and executes a query to get Integer results when one field is compared to others.
   *
   * @param leftEntityFieldPath A dot-chained path to a field.
   * @param rightEntityFieldPath A dot-chained path to a field.
   * @param conditions Query conditions to filter the selection.
   * @return A query single result - result String.
   */
  default <R> List<T> getStringComparedResults(
      String leftEntityFieldPath,
      Calculation calculation,
      It compared,
      Class<R> rightEntityType,
      String rightEntityFieldPath,
      Predicate... conditions) {

    Criteria<R> subEntityCriteria = new Criteria<>(getEntityManager(), rightEntityType);
    Subquery<String> subquery = getReadQuery().subquery(String.class);
    Root<R> subRoot = subquery.from(rightEntityType);
    Path<String> leftFieldPath = getPath(leftEntityFieldPath, getRoot());
    Path<String> rightFieldPath = subEntityCriteria.getPath(rightEntityFieldPath, subRoot);
    subquery.select(rightFieldPath);

    Predicate[] combinedConditions = null;
    switch (calculation) {
      case ALL:
        if (It.GREATER_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThan(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.LESS_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThan(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.GREATER_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThanOrEqualTo(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.LESS_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThanOrEqualTo(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().equal(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.NOT_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().notEqual(leftFieldPath, getBuilder().all(subquery));
        }
        break;
      case ANY:
        if (It.GREATER_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThan(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.LESS_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThan(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.GREATER_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThanOrEqualTo(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.LESS_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThanOrEqualTo(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().equal(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.NOT_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().notEqual(leftFieldPath, getBuilder().any(subquery));
        }
        break;
      default:
        break;
    }

    if (combinedConditions != null) {
      getReadQuery().select(getRoot()).where(combinedConditions);
    } else {
      getReadQuery().select(getRoot());
    }

    final TypedQuery<?> typedQuery = getEntityManager().createQuery(getReadQuery());
    List<T> results = (List<T>) typedQuery.getResultList();
    return results;
  }

  /**
   * Prepares and executes a query to get ZonedDateTime results when one field is compared to others.
   *
   * @param leftEntityFieldPath A dot-chained path to a field.
   * @param rightEntityFieldPath A dot-chained path to a field.
   * @param conditions Query conditions to filter the selection.
   * @return A query single result - result ZonedDateTime.
   */
  default <R> List<T> getZonedDateTimeComparedResults(
      String leftEntityFieldPath,
      Calculation calculation,
      It compared,
      Class<R> rightEntityType,
      String rightEntityFieldPath,
      Predicate... conditions) {

    Criteria<R> subEntityCriteria = new Criteria<>(getEntityManager(), rightEntityType);
    Subquery<ZonedDateTime> subquery = getReadQuery().subquery(ZonedDateTime.class);
    Root<R> subRoot = subquery.from(rightEntityType);
    Path<ZonedDateTime> leftFieldPath = getPath(leftEntityFieldPath, getRoot());
    Path<ZonedDateTime> rightFieldPath = subEntityCriteria.getPath(rightEntityFieldPath, subRoot);
    subquery.select(rightFieldPath);

    Predicate[] combinedConditions = null;
    switch (calculation) {
      case ALL:
        if (It.GREATER_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThan(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.LESS_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThan(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.GREATER_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThanOrEqualTo(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.LESS_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThanOrEqualTo(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().equal(leftFieldPath, getBuilder().all(subquery));
        }
        if (It.NOT_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().notEqual(leftFieldPath, getBuilder().all(subquery));
        }
        break;
      case ANY:
        if (It.GREATER_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThan(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.LESS_THAN.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThan(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.GREATER_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().greaterThanOrEqualTo(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.LESS_OR_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().lessThanOrEqualTo(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().equal(leftFieldPath, getBuilder().any(subquery));
        }
        if (It.NOT_EQUAL.equals(compared)) {
          combinedConditions = Arrays.copyOf(conditions, conditions.length + 1);
          combinedConditions[conditions.length] =
              getBuilder().notEqual(leftFieldPath, getBuilder().any(subquery));
        }
        break;
      default:
        break;
    }

    if (combinedConditions != null) {
      getReadQuery().select(getRoot()).where(combinedConditions);
    } else {
      getReadQuery().select(getRoot());
    }

    final TypedQuery<?> typedQuery = getEntityManager().createQuery(getReadQuery());
    List<T> results = (List<T>) typedQuery.getResultList();
    return results;
  }
}
