package com.zonzonzon.criteria_wrapper;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import org.slf4j.Logger;

/**
 * Methods providing filtering conditions for different clauses.
 *
 * @param <T> Type of entity for queries.
 */
public interface Common<T> {

  Logger getLog();

  EntityManager getEntityManager();

  void setCriteria(Criteria<T> criteriaRepository);

  Criteria<T> getCriteria();

  CriteriaBuilder getBuilder();

  CriteriaQuery<T> getReadQuery();

  void setReadQuery(CriteriaQuery<T> readQuery);

  Root<T> getRoot();

  void setRoot(Root<T> root);

  /** Resets current query settings to be ready for a new one. */
  default void resetQuery() {
    setCriteria(new Criteria<>(getEntityManager(), getType()));
  }

  Class<T> getType();

  Cache<T> getCache();

  void setCache(Cache<T> cache);

  /**
   * Generates a Criteria path from a chained string.
   *
   * @param nestedObjectValue A dot-chained path to a target field. The name of the root entity is
   *     omitted. An empty string should be passed to point to the root entity.
   * @param root A specific root for operations on some field type. NULL or entity root for other
   *     cases.
   * @param <F> A field type to operate on.
   * @return A path to a target field or entity.
   */
  default <F> Path<F> getPath(String nestedObjectValue, Root<T> root) {
    List<String> fieldNames = asList(nestedObjectValue.split("\\."));
    final List<Path<?>> paths = new ArrayList<>();
    paths.add(root != null ? root : getRoot());
    fieldNames.forEach(fieldName -> paths.add(paths.get(paths.size() - 1).get(fieldName)));
    Path<F> endFieldPath = (Path<F>) paths.get(paths.size() - 1);
    return endFieldPath;
  }

  /** An alias for {@link Common#getPath(String, Root)} */
  default Path<?> getPath(String nestedObjectValue) {
    return getPath(nestedObjectValue, null);
  }

  /** Builds a path to the field in embedded entity. */
  default Path getPath(SingularAttribute<T, ?>[] entityFields) {
    Path path = null;
    if (entityFields.length == 1) {
      // Entity and root field:
      path = getRoot().get(entityFields[0]);
    }
    if (entityFields.length > 1) {
      // Field in subentity:
      path = getRoot().join(entityFields[0]);
      for (int entityFieldsIndex = 1;
          entityFieldsIndex < entityFields.length;
          entityFieldsIndex++) {
        boolean isLast = entityFieldsIndex + 1 == entityFields.length;
        if (isLast) {
          path = path.get(entityFields[entityFieldsIndex]);
        } else {
          path = getRoot().join(entityFields[entityFieldsIndex]);
        }
      }
    }
    return path;
  }

  /**
   * Method decorator. Wraps method into a separate transaction to map entity.
   *
   * @param consumer Function to wrap. Accepts several input parameters and returns an object.
   * @param parameters Parameter values to be passed to a function.
   */
  default void runInTransaction(Consumer<List<?>> consumer, List<?> parameters) {

    EntityTransaction transaction = getEntityManager().getTransaction();
    transaction.begin();
    consumer.accept(parameters);
    transaction.commit();
  }
}
