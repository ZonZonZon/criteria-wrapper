package com.zonzonzon.criteria_wrapper.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Matching singular and plural noun forms.
 */
@AllArgsConstructor
@Getter
public enum NounFormEnum {

  NAME("name", "names");

  public final String singular;
  public final String plural;
}
