package com.zonzonzon.criteria_wrapper.utils;

import static com.zonzonzon.criteria_wrapper.utils.StringUtils.getterToCamelCase;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/** Helper methods to work with reflection. */
@UtilityClass
@Slf4j
public class ReflectionUtils {
  /**
   * Executes a chained call without a risk of exceptions: NullPointerException,
   * LazyInitializationException or any other. Also interprets as null Strings with values like
   * "null", "Null" and "NULL".
   *
   * @param statement A chain to be checked for null pointer and executed.
   * @param <T> Type to return.
   * @return Returns an actual value of the specified type if all the objects in the chain are not
   *     null. Returns null if any of the objects in the chain are null.
   */
  public static <T> T getSafe(Supplier<T> statement) {
    try {
      T value = statement.get();

      if (value instanceof String
          && (String.valueOf(value).equals("")
          || String.valueOf(value).equals("null")
          || String.valueOf(value).equals("Null")
          || String.valueOf(value).equals("NULL"))) {

        return null;
      } else {
        return value;
      }
    } catch (Exception anyException) {
      return null;
    }
  }

  /**
   * Detects if class has the expected field. Also looks for fields in the embedded objects when
   * parameters are passed using dots.
   *
   * @param aClass A class to search in.
   * @param singularFieldName Expected field name singular notation. If not found - a simple plural
   *     notation with "s" will be tried. Use dots for embedded object fields.
   * @param specialFieldName Full getter method name. Use dots for embedded object fields.
   * @return Class has such a field.
   */
  @SneakyThrows
  public static boolean hasField(
      Class<?> aClass, String singularFieldName, String specialFieldName) {

    // Singular check:
    if (singularFieldName != null && singularFieldName.contains(".")) {
      List<String> fieldsChain = List.of(singularFieldName.split("\\."));
      Class<?> currentClass = aClass;
      Iterator<String> iterator = fieldsChain.iterator();
      while (iterator.hasNext()) {
        String currentField = iterator.next();
        boolean isFinalField = !iterator.hasNext();

        Method getter =
            isFinalField && specialFieldName != null
                ? getGetter(currentClass, null, specialFieldName)
                : getGetter(currentClass, currentField, null);

        if (getter != null && !isFinalField) {
          currentClass = getter.getReturnType();
          continue;
        }
        return getter != null;
      }
    } else {
      Method getter = getGetter(aClass, singularFieldName, specialFieldName);
      return getter != null;
    }
    return false;
  }

  private static boolean isPublicGetter(Method method, Class<?> aClass) {
    if (!aClass.isEnum() && !method.getName().startsWith("get")) {
      return false;
    }

    if (method.getParameterTypes().length != 0) {
      return false;
    }

    if (void.class.equals(method.getReturnType())) {
      return false;
    }

    if (!Modifier.isPublic(method.getModifiers())) {
      return false;
    }

    if (Modifier.isNative(method.getModifiers())) {
      return false;
    }

    return true;
  }

  public static List<Method> getPublicGetters(Class<?> aClass) {
    List<Method> getterList = new ArrayList<>();
    Method[] methods = aClass.getMethods();

    for (Method method : methods) {
      if (isPublicGetter(method, aClass)) {
        getterList.add(method);
      }
    }

    return getterList;
  }

  /** Finds getter by field name. See {@link ReflectionUtils#hasField(Class, String, String)} */
  public static Method getGetter(
      Class<?> aClass, String singularFieldName, String specialFieldName) {

    return getPublicGetters(aClass).stream()
        .map(
            method -> {
              String getterName = getterToCamelCase(method.getName());

              String singularGetter = getterName.equals(singularFieldName) ? getterName : null;

              String pluralGetterName =
                  singularGetter == null && getterName.equals(singularFieldName + "s")
                      ? getterName
                      : null;

              try {
                pluralGetterName =
                    pluralGetterName == null
                        && getterName.equals(
                        NounFormEnum.valueOf(singularFieldName).getPlural())
                        ? getterName
                        : pluralGetterName;
              } catch (Exception ignore) {
              }

              String specialGetterName =
                  method.getName().equals(specialFieldName) ? method.getName() : null;

              boolean hasField =
                  Stream.of(singularGetter, pluralGetterName, specialGetterName)
                      .filter(Objects::nonNull)
                      .findFirst()
                      .orElse(null)
                      != null;

              if (hasField) {
                return method;
              } else {
                return null;
              }
            })
        .filter(Objects::nonNull)
        .findFirst()
        .orElse(null);
  }

  /**
   * Method to check if a Class is a Collection or not.
   *
   * @param aClass Class to inspect.
   * @return Class extends or implements Collection.
   */
  public static boolean isClassCollection(Class<?> aClass) {
    return Collection.class.isAssignableFrom(aClass) || Map.class.isAssignableFrom(aClass);
  }

  /**
   * Get type of collection field.
   *
   * @param aClass A class containing collection.
   * @param collectionName A collection field name.
   */
  @SneakyThrows
  public static Class<?> getCollectionType(Class<?> aClass, String collectionName) {
    Field field = aClass.getDeclaredField(collectionName);
    ParameterizedType genericType = (ParameterizedType) field.getGenericType();
    Class<?> typeClass = (Class<?>) genericType.getActualTypeArguments()[0];
    return typeClass;
  }
}
