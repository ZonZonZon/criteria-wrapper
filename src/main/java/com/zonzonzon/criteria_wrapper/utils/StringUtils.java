package com.zonzonzon.criteria_wrapper.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/** Helper methods to work with strings. */
@UtilityClass
@Slf4j
public class StringUtils {
  /**
   * Derives fieldName from getter method name, if getter follows the conventions.
   *
   * @param getterName Getter method name.
   * @return Field name the getter refers to.
   */
  public static String getterToCamelCase(String getterName) {
    return getterName.replaceFirst("get", "").substring(0, 1).toLowerCase()
        + getterName.replaceFirst("get", "").substring(1);
  }
}
