package com.zonzonzon.criteria_wrapper;

import static com.zonzonzon.criteria_wrapper.utils.ReflectionUtils.getCollectionType;
import static com.zonzonzon.criteria_wrapper.utils.ReflectionUtils.getGetter;
import static com.zonzonzon.criteria_wrapper.utils.ReflectionUtils.getSafe;
import static com.zonzonzon.criteria_wrapper.utils.ReflectionUtils.isClassCollection;
import static com.zonzonzon.criteria_wrapper.utils.StringUtils.getterToCamelCase;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import javax.persistence.AttributeNode;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityGraph;
import javax.persistence.Subgraph;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Path;
import javax.persistence.metamodel.SingularAttribute;

/**
 * Methods providing fetch graph operations.
 *
 * @param <T> Type of entity for queries.
 */
public interface Graph<T> extends Common<T> {

  /**
   * Creates Typed Criteria query.
   *
   * @param cached Marks query with a hint to cache.
   * @param embedded See {@link Result#EMBEDDED_DESCRIPTION}
   */
  default TypedQuery<T> createTypedQueryAndFetchGraph(Boolean cached, List<String> embedded) {

    TypedQuery<T> typedQuery = getEntityManager().createQuery(getReadQuery());
    if (cached != null && cached) {
      typedQuery = typedQuery.setHint("javax.persistence.cache.storeMode", CacheStoreMode.USE);
    }

    if (embedded != null && !embedded.isEmpty()) {
      EntityGraph<?> entityGraph = buildGraphFromEmbedded(embedded);
      typedQuery = typedQuery.setHint("javax.persistence.fetchgraph", entityGraph);
    }

    return typedQuery;
  }

  /**
   * Builds dynamic entity graph based on given embedded objects. If objects from embedded don't
   * exist, then they are omitted from the entity fetch graph.
   *
   * @param embedded See {@link Result#EMBEDDED_DESCRIPTION}
   * @return Entity fetch graph with a structure matching embedded.
   */
  default EntityGraph<?> buildGraphFromEmbedded(List<String> embedded) {
    EntityGraph<?> graph = getEntityManager().createEntityGraph(getType());
    embedded.forEach(
        embeddable -> {
          if (embeddable != null && embeddable.contains(".")) {
            List<String> fieldsChain = List.of(embeddable.split("\\."));
            // Each embeddable chain starts at the root entity:
            Class<?>[] currentClass = {getType()};
            Subgraph<?> subgraph;
            // Iterate embedded chain fields:
            Iterator<String> iterator = fieldsChain.iterator();
            boolean isFirstChainPart = true;
            while (iterator.hasNext()) {
              String[] currentField = {iterator.next()};
              boolean isFinalChainField = !iterator.hasNext();
              Method getter = getGetter(currentClass[0], currentField[0], null);
              boolean fieldExistsInEntity = getter != null;
              if (fieldExistsInEntity) {
                currentField[0] = getterToCamelCase(getter.getName().replaceFirst("get", ""));
                subgraph = getSubgraph(graph, currentClass[0], currentField[0], fieldsChain);

                boolean isExistingSubgraph =
                    subgraph != null && subgraph.getClassType().getName().equals(currentField[0]);

                boolean isParallelSubgraph =
                    subgraph != null
                        && !subgraph.getClassType().getName().equals(currentClass[0].getName());

                boolean addToGraph = currentClass[0].getName().equals(getType().getName());

                if (isFinalChainField) {
                  // Add end attribute:
                  if ((subgraph == null && isFirstChainPart)
                      || (isParallelSubgraph && isFirstChainPart)) {
                    graph.addAttributeNodes(currentField[0]);
                  } else if (subgraph != null) {
                    subgraph.addAttributeNodes(currentField[0]);
                  }
                } else {
                  // Add subgraph:
                  if (subgraph == null && isFirstChainPart) {
                    graph.addSubgraph(currentField[0]);
                  } else if (subgraph != null && !isExistingSubgraph && !isParallelSubgraph) {
                    subgraph.addSubgraph(currentField[0]);
                  }
                  // Switch entity to embed into:
                  currentClass[0] = getter.getReturnType();
                  if (isClassCollection(currentClass[0])) {
                    currentClass[0] =
                        getCollectionType(getter.getDeclaringClass(), currentField[0]);
                  }
                }
              }
              isFirstChainPart = false;
            }
          } else {
            // Add end entity to graph root:
            Method getter = getSafe(() -> getGetter(getType(), embeddable, null));
            if (getter != null) {
              getSafe(
                  () -> {
                    graph.addAttributeNodes(embeddable);
                    return null;
                  });
            }
          }
        });
    return graph;
  }

  /**
   * Finds a subgraph in a graph. Looks for specific subgraph location following given fields chain
   * path. Returns the latest subgraph found by path, so for a final part a previous part subgraph
   * is returned.
   *
   * @param graph Graph to look in.
   * @param classToEmbedInto A class to embed to.
   * @param embeddedField A field to be fully fetched for this entity.
   * @return A subgraph.
   */
  default Subgraph<?> getSubgraph(
      EntityGraph<?> graph,
      Class<?> classToEmbedInto,
      String embeddedField,
      List<String> fieldsChain) {

    final Subgraph<?>[] subgraph = new Subgraph<?>[1];
    final List<AttributeNode<?>>[] attributeNodes = new List[] {graph.getAttributeNodes()};

    fieldsChain.forEach(
        field -> {
          Subgraph<?> subgraphFound = getSubgraphByPath(attributeNodes[0], field);
          if (subgraphFound != null) {
            subgraph[0] = subgraphFound;
            attributeNodes[0] = subgraph[0].getAttributeNodes();
          }
        });
    return subgraph[0];
  }

  default Subgraph getSubgraphByPath(List<AttributeNode<?>> graphAttributeNodes, String pathPart) {
    Subgraph subgraphFound =
        graphAttributeNodes.stream()
            .filter(attributeNode -> attributeNode.getAttributeName().equals(pathPart))
            .flatMap(attributeNode -> attributeNode.getSubgraphs().values().stream())
            .findAny()
            .orElse(null);
    return subgraphFound;
  }

  /**
   * Builds a path to an embedded entity field. Is currently implemented for two levels of
   * propagation.
   *
   * @param entities Entities in the order they are embedded into each other. The last one contains
   *     the target value.
   * @return Path to subentity field.
   */
  default Path getNested(SingularAttribute<?, ?>... entities) {
    Path path;
    //    path = getRoot().get(entities[0]).get(entities[1]);
    //    if (entities.length > 0) {
    //      path = getRoot().get(entities[0]);
    //      for (int entityIndex = 1; entityIndex < entities.length; entityIndex++) {
    //        path = path.get(entities[entityIndex]);
    //      }
    //    }
    //    return path;
    return null;
  }
}
