package com.zonzonzon.criteria_wrapper;

public enum It {
    EQUAL,
    NOT_EQUAL,
    GREATER_THAN,
    LESS_THAN,
    GREATER_OR_EQUAL,
    LESS_OR_EQUAL,
    IN,
    CONTAIN;
}
