package com.zonzonzon.criteria_wrapper.exception;

public class NoQueryExpressionException extends RuntimeException {

  public static final String MESSAGE = "Query expression type is not set";
}
