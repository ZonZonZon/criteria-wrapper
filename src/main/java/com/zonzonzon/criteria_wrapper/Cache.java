package com.zonzonzon.criteria_wrapper;

import java.util.List;

/**
 * Methods providing cache operations.
 *
 * @param <T> Type of entity for queries.
 */
public interface Cache<T> extends Common<T> {

  Cache<T> getInvalidated();
  boolean isCached(String sqlString);
  Object load(String sqlString);
  void save(String sqlString, Object response);

  /**
   * Cache clearance.
   *
   * @param invalidatedClasses Classes to clear cache for. Clear all Hibernate Level 2 cache if
   *     NULL.
   */
  default void invalidateCache(List<Class<T>> invalidatedClasses) {
    if (invalidatedClasses == null) {
      getEntityManager().getEntityManagerFactory().getCache().evictAll();
      setCache(getCache().getInvalidated());
    } else {
      for (Class<T> invalidatedClass : invalidatedClasses) {
        getEntityManager().getEntityManagerFactory().getCache().evict(invalidatedClass);
      }
    }
  }
}
