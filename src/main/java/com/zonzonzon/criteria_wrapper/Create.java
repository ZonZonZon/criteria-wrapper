package com.zonzonzon.criteria_wrapper;

import java.util.List;

/**
 * Methods providing create operations.
 *
 * @param <T> Type of entity for queries.
 */
public interface Create<T> extends Common<T> {

  /**
   * Persist an entity.
   *
   * @param entity Entity instance.
   */
  default void create(T entity) {
    getEntityManager().persist(entity);
    resetQuery();
  }

  /**
   * Persist an entity in an isolated transaction.
   *
   * @param entity Entity instance.
   */
  default void createInTransaction(T entity) {
    runInTransaction(
        parameters -> getEntityManager().persist(parameters.get(0)), List.of(entity));

    resetQuery();
  }
}
