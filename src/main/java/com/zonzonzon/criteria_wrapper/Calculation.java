package com.zonzonzon.criteria_wrapper;

enum Calculation {
  SUM,
  AVERAGE,
  MAX,
  MIN,
  COUNT,
  COUNT_DISTINCT,
  ALL,
  ANY
}
