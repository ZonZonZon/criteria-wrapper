package com.zonzonzon.criteria_wrapper;

import static com.zonzonzon.criteria_wrapper.Result.EMBEDDED_QUERY_FAILURE;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

/**
 * Methods providing pagination operations.
 *
 * @param <T> Type of entity for queries.
 */
public interface Page<T> extends Common<T>, Graph<T> {

  /**
   * Counts total number of resources for pagination needs.
   *
   * @param predicateArray Query conditions to filter out resources. May be null.
   * @return Number of resources.
   */
  default Long getTotalCount(Class<T> entityClass, Predicate[] predicateArray) {
    CriteriaQuery<Long> query = getBuilder().createQuery(Long.class);
    query.select(getBuilder().countDistinct(query.from(entityClass)));
    if (predicateArray != null) {
      query.where(predicateArray);
    }
    return getEntityManager().createQuery(query).getSingleResult();
  }

  /**
   * Get one page of resources.
   *
   * @param cached Cache results.
   * @param embedded See {@link Result#EMBEDDED_DESCRIPTION}
   * @param pageRequest Pagination information.
   * @return A page of resources.
   */
  default PageImpl<T> getPage(Boolean cached, List<String> embedded, PageRequest pageRequest) {
    TypedQuery<?> typedQuery = createTypedQueryAndFetchGraph(cached, embedded);
    typedQuery.setFirstResult(pageRequest.getPageNumber() * pageRequest.getPageSize());
    typedQuery.setMaxResults(pageRequest.getPageSize());
    try {
      List<T> resultList =
          typedQuery.getResultList().stream()
              .map(resource -> (T) resource)
              .collect(Collectors.toList());
      T exampleResource = resultList.stream().findAny().orElse(null);
      resetQuery();
      return new PageImpl<>(
          resultList, pageRequest, getTotalCount((Class<T>) exampleResource.getClass(), null));
    } catch (Exception brokenData) {
      throw new RuntimeException(EMBEDDED_QUERY_FAILURE + brokenData.getLocalizedMessage());
    }
  }
}
