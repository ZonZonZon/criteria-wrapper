package com.zonzonzon.criteria_wrapper;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

/**
 * A Facade wrapper to simplify Criteria API usage. It is expected that one instance of this class
 * is used for one query. Query configurations are accumulated and reset after the query is applied.
 *
 * @author Andrey Zonin - Open Source Library.
 */
// @RequiredArgsConstructor
  @Slf4j
class Criteria<T>
    implements Create<T>,
        Graph<T>,
        Read<T>,
        Filter<T>,
        Sort<T>,
        Page<T>,
        Result<T>,
        Update<T>,
        Delete<T> {

  /** Amount of queries in one batch operation (Hibernate Level 1 clearance). */
  public static final int QUERIES_BATCH_SIZE = 30;

  /** Amount of records in one SQL-query. */
  public static final int INSERT_STATEMENTS_BULK_SIZE = 100;

  /** A response structure to contain resources aside with pagination information. */
  @Getter
  @Setter
  @NoArgsConstructor
  public static class Paginated<R> {
    private Integer totalElements;
    private Integer totalPages;
    private R resource;
  }

  @Getter public final EntityManager entityManager;
  @Getter private final Class<T> type;
  private Criteria<T> criteria;
  private CriteriaBuilder builder;
  @Setter private CriteriaQuery<T> readQuery;
  @Setter private CriteriaUpdate<T> updateQuery;
  @Setter private CriteriaDelete<T> deleteQuery;
  @Setter private Root<T> root;
  @Getter @Setter private Cache<T> cache;

  /**
   * A constructor to create a new instance for queries.
   *
   * @param entityManager Injected Entity Manager to be used.
   * @param type Entity type for queries.
   */
  public Criteria(EntityManager entityManager, Class<T> type) {
    this.entityManager = entityManager;
    this.type = type;
    this.criteria = this;
    getBuilder();
    this.getReadQuery();
    this.getUpdateQuery();
    this.getDeleteQuery();
    getRoot();
  }

  public void setCriteria(Criteria<T> criteria){
    this.criteria = criteria;
  }

  public Criteria<T> getCriteria() {
    return criteria;
  }

  public Logger getLog(){
    return log;
  }

  public CriteriaBuilder getBuilder() {
    builder = builder != null ? builder : entityManager.getCriteriaBuilder();
    return builder;
  }

  public CriteriaQuery<T> getReadQuery() {
    readQuery = readQuery != null ? readQuery : getBuilder().createQuery(type);
    return readQuery;
  }

  public CriteriaUpdate<T> getUpdateQuery() {
    updateQuery = updateQuery != null ? updateQuery : getBuilder().createCriteriaUpdate(type);
    return updateQuery;
  }

  public CriteriaDelete<T> getDeleteQuery() {
    deleteQuery = deleteQuery != null ? deleteQuery : getBuilder().createCriteriaDelete(type);
    return deleteQuery;
  }

  public Root<T> getRoot() {
    root = root != null ? root : this.getReadQuery().from(type);
    return root;
  }
}
