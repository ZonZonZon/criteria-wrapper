package com.zonzonzon.criteria_wrapper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CacheStoreMode;
import javax.persistence.EntityGraph;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * Methods providing result operations.
 *
 * @param <T> Type of entity for queries.
 */
public interface Result<T> extends Common<T>, Graph<T> {

  String EMBEDDED_QUERY_FAILURE =
      "Can't parse data from typed query. "
          + "You may have broken data "
          + "like references to non-existent embedded entities in your graph. ";

  String SET_LAZY_LOADS_WARNING =
      "Do not use set results with embedded, unless you do it in a global transaction and\n"
          + " lazily parse all the embedded entities (a lot of JPA subrequests). Set type will force to\n"
          + " read all the fields and so to lazily initialize all of them.";

  /**
   * A list of embedded entity names to be fetched together with this entity. The more subentities
   * are fetched - the heavier is the query. While subentities will be fetched recursively many
   * levels in depth, you only have to declare two placeholders dot-separated. In embedded list use
   * field names as they are declared in the class: singular-plural forms conversions are set up in
   * a special enum for your entities. Example of embedded list: [a.b, b.c, c.d].
   */
  String EMBEDDED_DESCRIPTION = "";

  /** Marks query with a hint to cache. */
  String CACHED_DESCRIPTION = "";

  /**
   * Creates Typed Criteria query.
   *
   * @param cached Marks query with a hint to cache.
   * @param entityGraphName Marks query with a hint for specific entity graph fetching setting.
   */
  default TypedQuery<T> createTypedQuery(Boolean cached, String entityGraphName) {
    TypedQuery<T> typedQuery = getEntityManager().createQuery(getReadQuery());
    if (cached != null && cached) {
      typedQuery = typedQuery.setHint("javax.persistence.cache.storeMode", CacheStoreMode.USE);
    }

    if (entityGraphName != null) {
      EntityGraph<?> entityGraph = getEntityManager().getEntityGraph(entityGraphName);
      typedQuery = typedQuery.setHint("javax.persistence.fetchgraph", entityGraph);
    }
    return typedQuery;
  }

  /** See {@link Result#createTypedQuery(Boolean, String)} */
  default TypedQuery<T> createTypedQuery(Boolean cached) {
    return createTypedQuery(cached != null && cached, null);
  }

  /**
   * Return query result. Method builds the query.
   *
   * @param cached Marks query with a hint to cache.
   * @param entityGraphName Marks query with a hint for specific entity graph fetching setting.
   * @return Results list.
   */
  default List<T> getResultList(Boolean cached, String entityGraphName) {
    TypedQuery<T> typedQuery = createTypedQuery(cached, entityGraphName);
    List<T> resultList = typedQuery.getResultList();
    resetQuery();
    return resultList;
  }

  /**
   * @deprecated See {@link Result#SET_LAZY_LOADS_WARNING}
   * @return Result stream.
   */
  @Deprecated(forRemoval = false)
  default Set<T> getResultSet(Boolean cached, List<String> embedded) {
    try {
      List<T> resultList = getResultList(cached, embedded);
      resetQuery();
      return new HashSet<>(resultList);
    } catch (Exception brokenData) {
      throw new RuntimeException(EMBEDDED_QUERY_FAILURE + brokenData.getLocalizedMessage());
    }
  }

  /**
   * Return query result. Method builds the query and a respective fetch graph for entity based on
   * embedded objects list.
   *
   * @param cached See {@link Result#CACHED_DESCRIPTION}
   * @param embedded See {@link Result#EMBEDDED_DESCRIPTION}
   * @return Result stream.
   */
  default List<T> getResultList(Boolean cached, List<String> embedded) {
    TypedQuery<T> typedQuery = createTypedQueryAndFetchGraph(cached, embedded);
    try {
      resetQuery();
      return typedQuery.getResultList();
    } catch (Exception brokenData) {
      throw new RuntimeException(EMBEDDED_QUERY_FAILURE + brokenData.getLocalizedMessage());
    }
  }

  /** See {@link Result#getResultList(Boolean, String)} */
  default List<T> getResultList(Boolean cached) {
    List<T> resultList = createTypedQuery(cached).getResultList();
    resetQuery();
    return resultList;
  }

  /** See {@link Result#getResultList(Boolean, String)} */
  default List<T> getResultList() {
    List<T> resultList = createTypedQuery().getResultList();
    resetQuery();
    return resultList;
  }

  /**
   * Return query result. Method builds the query.
   *
   * @param cached See @{@link Result#CACHED_DESCRIPTION}
   * @param entityGraphName Marks query with a hint for specific entity graph fetching setting.
   * @deprecated See {@link Result#SET_LAZY_LOADS_WARNING}
   * @return Results list.
   */
  @Deprecated(forRemoval = false)
  default Set<T> getResultSet(Boolean cached, String entityGraphName) {
    List<T> resultList = getResultList(cached, entityGraphName);
    Set<T> resultSet = new HashSet<>(resultList);
    resetQuery();
    return resultSet;
  }

  /**
   * @deprecated See {@link Result#SET_LAZY_LOADS_WARNING}
   */
  @Deprecated(forRemoval = false)
  default Set<T> getResultSet(Boolean cached) {
    List<T> resultList = getResultList(cached);
    Set<T> resultSet = new HashSet<>(resultList);
    resetQuery();
    return resultSet;
  }

  /**
   * @deprecated See {@link Result#SET_LAZY_LOADS_WARNING}
   */
  @Deprecated(forRemoval = false)
  default Set<T> getResultSet() {
    List<T> resultList = getResultList();
    Set<T> resultSet = new HashSet<>(resultList);
    resetQuery();
    return resultSet;
  }

  /**
   * Get a single result. Builds a query.
   *
   * @param cached See @{@link Result#CACHED_DESCRIPTION}
   * @param entityGraphName Marks query with a hint for specific entity graph fetching setting.
   * @return A single result.
   */
  default T getSingleResult(Boolean cached, String entityGraphName) {
    EntityGraph<?> entityGraph = getEntityManager().getEntityGraph(entityGraphName);
    TypedQuery<T> typedQuery = createTypedQuery(cached, entityGraphName);
    typedQuery.setHint("javax.persistence.fetchgraph", entityGraph);
    T singleResult = typedQuery.getSingleResult();
    resetQuery();
    return singleResult;
  }

  /** See {@link Result#getSingleResult (Boolean, String)} */
  default T getSingleResult(Boolean cached) {
    T result = createTypedQuery(cached).getSingleResult();
    resetQuery();
    return result;
  }

  /** See {@link Result#getSingleResult (Boolean, String)} */
  default T getSingleResult() {
    T result = createTypedQuery().getSingleResult();
    resetQuery();
    return result;
  }

  /**
   * Uncached query.
   *
   * <p>См. {@link Result#createTypedQuery(Boolean)}
   *
   * @return Typed query.
   */
  default TypedQuery<T> createTypedQuery() {
    return createTypedQuery(false);
  }

  /**
   * Get cached result or make a query. Is used for native queries.
   *
   * @param sqlString Native query text.
   * @return A single result.
   */
  default Object getSingleResult(String sqlString) {
    Object response = null;
    if (getCache().isCached(sqlString)) {
      // From cache:
      response = getCache().load(sqlString);
    } else {
      // Database query:
      try {
        Query query = getEntityManager().createNativeQuery(sqlString);
        response = query.getSingleResult();
        getCache().save(sqlString, response);
      } catch (Exception ignore) {
        // Not found.
      }
    }
    resetQuery();
    return response;
  }
}
