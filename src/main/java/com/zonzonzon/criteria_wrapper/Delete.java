package com.zonzonzon.criteria_wrapper;

/**
 * Methods providing delete operations.
 *
 * @param <T> Type of entity for queries.
 */
public interface Delete<T> extends Common<T> {

  /** Example of optimized batch entity deletion. */
  default void deleteMany(Object entity) {
    // TODO:
    resetQuery();
  }
}
