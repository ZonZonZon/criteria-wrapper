package com.zonzonzon.criteria_wrapper;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.metamodel.SingularAttribute;

/**
 * Methods providing sort operations.
 *
 * @param <T> Type of entity for queries.
 */
public interface Sort<T> extends Common<T> {

  /**
   * SQL-like ORDER BY expression. Is added to the query of current Criteria instance.
   *
   * @param expression Expression to parse data of certain type.
   * @param smallToBig Sorting direction is small to big (ASC). Otherwise - big to small (DESC).
   * @return Current Criteria instance.
   */
  default Criteria<T> orderByCommon(Expression<?> expression, boolean smallToBig) {
    if (smallToBig) {
      getReadQuery().orderBy(getBuilder().asc(expression));
    } else {
      getReadQuery().orderBy(getBuilder().desc(expression));
    }
    return getCriteria();
  }

  default Criteria<T> orderByObject(SingularAttribute<T, Object> entityField, boolean smallToBig) {
    return orderByCommon(getRoot().get(entityField), smallToBig);
  }

  default Criteria<T> orderByInteger(
      SingularAttribute<T, Integer> entityField, boolean smallToBig) {
    return orderByCommon(getRoot().get(entityField), smallToBig);
  }

  default Criteria<T> orderByString(SingularAttribute<T, String> entityField, boolean smallToBig) {
    return orderByCommon(getRoot().get(entityField), smallToBig);
  }

  default Criteria<T> orderByDouble(SingularAttribute<T, Double> entityField, boolean smallToBig) {
    return orderByCommon(getRoot().get(entityField), smallToBig);
  }

  default Criteria<T> orderByBoolean(
      SingularAttribute<T, Boolean> entityField, boolean smallToBig) {
    return orderByCommon(getRoot().get(entityField), smallToBig);
  }

  default Criteria<T> orderByList(List<Order> orders) {
    getReadQuery().orderBy(orders);
    return getCriteria();
  }

  default Criteria<T> orderByLocalDate(
      SingularAttribute<T, LocalDate> entityField, boolean smallToBig) {

    return orderByCommon(getRoot().get(entityField), smallToBig);
  }
}
