package com.zonzonzon.criteria_wrapper;

import com.zonzonzon.criteria_wrapper.exception.NoQueryExpressionException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;

/**
 * Methods providing read operations.
 *
 * @param <T> Type of entity for queries.
 */
public interface Read<T> extends Common<T> {

  /**
   * SELECT expression. Used to resemble SQL. There is no need call it implicitly as it is always
   * called by default.
   *
   * @param fields Fields to select. These fields should be present in entity constructor, otherwise
   *     an error will arise: "PropertyNotFoundException: no appropriate constructor in class". If
   *     no fields are set - all fields are returned.
   * @return Current Criteria instance.
   */
  default Criteria<T> select(SingularAttribute<T, ?>... fields) {
    if (fields.length == 0) {
      return selectAll();
    }
    List<Selection<?>> selectedFields = new ArrayList<>();
    for (SingularAttribute<T, ?> field : fields) {
      selectedFields.add(getRoot().get(field));
    }
    getReadQuery().multiselect(selectedFields);
    return getCriteria();
  }

  /** SELECT * expression. Select all fields. */
  default Criteria<T> selectAll() {
    setReadQuery(getReadQuery().select(getRoot()));
    return getCriteria();
  }

  /**
   * SQL-like DISTINCT expression.
   *
   * @return Current Criteria instance.
   */
  default Criteria<T> distinct() {
    getReadQuery().distinct(true);
    return getCriteria();
  }

  /**
   * SQL-like JOIN expression.
   *
   * @return Current Criteria instance.
   */
  default <S> Criteria<T> join(ListAttribute<T, S> oneToManyValues) {
    //    getQuery().select(getRoot().join(oneToManyValues));
    return getCriteria();
  }

  /**
   * SQL-like WHERE expression. Is added to current Criteria instance.
   *
   * @param conditions Filtering conditions.
   * @return Current Criteria instance.
   */
  default Criteria<T> where(Predicate... conditions) {
    getReadQuery().where(conditions);
    return getCriteria();
  }

  /**
   * Prepares and executes a query to get Integer result.
   *
   * @param entityFieldPath A dot-chained path to a field.
   * @param conditions Query conditions to filter the selection.
   * @return A query single result - result Integer.
   */
  default Integer getIntegerResult(
      Calculation calculation, String entityFieldPath, Predicate... conditions) {

    final CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<Integer> query = builder.createQuery(Integer.class);
    Root<T> root = query.from(getType());
    Path<Integer> fieldPath = getPath(entityFieldPath, root);

    Expression<Integer> expression = null;
    switch (calculation) {
      case SUM:
        expression = getBuilder().sum(fieldPath);
        break;
      case MAX:
        expression = getBuilder().max(fieldPath);
        break;
      case MIN:
        expression = getBuilder().min(fieldPath);
        break;
      default:
        {
          break;
        }
    }
    if (expression != null) {
      if (conditions != null && conditions.length > 0) {
        query.select(expression).where(conditions);
      } else {
        query.select(expression);
      }
    } else {
      throw new NoQueryExpressionException();
    }
    final TypedQuery<?> typedQuery = getEntityManager().createQuery(query);
    Integer singleResult = (Integer) typedQuery.getSingleResult();
    return singleResult;
  }

  /**
   * Prepares and executes a query to get Double result.
   *
   * @param entityFieldPath A dot-chained path to a field.
   * @param conditions Query conditions to filter the selection.
   * @return A query single result - Double.
   */
  default Double getDoubleResult(
      Calculation calculation, String entityFieldPath, Predicate... conditions) {

    final CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<Double> query = builder.createQuery(Double.class);
    Root<T> root = query.from(getType());
    Path<Double> fieldPath = getPath(entityFieldPath, root);

    Expression<Double> expression = null;
    switch (calculation) {
      case SUM:
        expression = getBuilder().sum(fieldPath);
        break;
      case MAX:
        expression = getBuilder().max(fieldPath);
        break;
      case MIN:
        expression = getBuilder().min(fieldPath);
        break;
      case AVERAGE:
        expression = getBuilder().avg(fieldPath);
        break;
      default:
        {
          break;
        }
    }
    if (expression != null) {
      if (conditions != null && conditions.length > 0) {
        query.select(expression).where(conditions);
      } else {
        query.select(expression);
      }
    } else {
      throw new NoQueryExpressionException();
    }
    final TypedQuery<?> typedQuery = getEntityManager().createQuery(query);
    Double singleResult = (Double) typedQuery.getSingleResult();
    return singleResult;
  }

  /**
   * Prepares and executes a query to get Long result.
   *
   * @param entityFieldPath A dot-chained path to a field.
   * @param conditions Query conditions to filter the selection.
   * @return A query single result - Long.
   */
  default Long getLongResult(
      Calculation calculation, String entityFieldPath, Predicate... conditions) {

    final CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<Long> query = builder.createQuery(Long.class);
    Root<T> root = query.from(getType());
    Path<Long> fieldPath = getPath(entityFieldPath, root);

    Expression<Long> expression = null;
    switch (calculation) {
      case SUM:
        expression = getBuilder().sum(fieldPath);
        break;
      case MAX:
        expression = getBuilder().max(fieldPath);
        break;
      case MIN:
        expression = getBuilder().min(fieldPath);
        break;
      case COUNT:
        expression = getBuilder().count(fieldPath);
        break;
      case COUNT_DISTINCT:
        expression = getBuilder().countDistinct(fieldPath);
        break;
      default:
        {
          break;
        }
    }
    if (expression != null) {
      if (conditions != null && conditions.length > 0) {
        query.select(expression).where(conditions);
      } else {
        query.select(expression);
      }
    } else {
      throw new NoQueryExpressionException();
    }
    final TypedQuery<?> typedQuery = getEntityManager().createQuery(query);
    Long singleResult = (Long) typedQuery.getSingleResult();
    return singleResult;
  }

  /**
   * Prepares and executes a query to get Long result.
   *
   * @param entityFieldPath A dot-chained path to a field.
   * @param conditions Query conditions to filter the selection.
   * @return A query single result - Long.
   */
  default ZonedDateTime getZonedDateTimeResult(
      Calculation calculation, String entityFieldPath, Predicate... conditions) {

    final CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<ZonedDateTime> query = builder.createQuery(ZonedDateTime.class);
    Root<T> root = query.from(getType());
    Path<ZonedDateTime> fieldPath = getPath(entityFieldPath, root);

    Expression<ZonedDateTime> expression = null;
    switch (calculation) {
      case MAX:
        expression = getBuilder().greatest(fieldPath);
        break;
      case MIN:
        expression = getBuilder().least(fieldPath);
        break;
      default:
        {
          break;
        }
    }
    if (expression != null) {
      if (conditions != null && conditions.length > 0) {
        query.select(expression).where(conditions);
      } else {
        query.select(expression);
      }
    } else {
      throw new NoQueryExpressionException();
    }
    final TypedQuery<?> typedQuery = getEntityManager().createQuery(query);
    ZonedDateTime singleResult = (ZonedDateTime) typedQuery.getSingleResult();
    return singleResult;
  }

  /**
   * Prepares and executes a query to get Long result.
   *
   * @param entityFieldPath A dot-chained path to a field.
   * @param conditions Query conditions to filter the selection.
   * @return A query single result - Long.
   */
  default String getStringResult(
      Calculation calculation, String entityFieldPath, Predicate... conditions) {

    final CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<String> query = builder.createQuery(String.class);
    Root<T> root = query.from(getType());
    Path<String> fieldPath = getPath(entityFieldPath, root);

    Expression<String> expression = null;
    switch (calculation) {
      case MAX:
        expression = getBuilder().greatest(fieldPath);
        break;
      case MIN:
        expression = getBuilder().least(fieldPath);
        break;
      default:
        {
          break;
        }
    }
    if (expression != null) {
      if (conditions != null && conditions.length > 0) {
        query.select(expression).where(conditions);
      } else {
        query.select(expression);
      }
    } else {
      throw new NoQueryExpressionException();
    }
    final TypedQuery<?> typedQuery = getEntityManager().createQuery(query);
    String singleResult = (String) typedQuery.getSingleResult();
    return singleResult;
  }
}
