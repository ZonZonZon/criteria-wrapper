package com.zonzonzon.criteria_wrapper;

import static com.zonzonzon.criteria_wrapper.Calculation.AVERAGE;
import static com.zonzonzon.criteria_wrapper.Calculation.COUNT;
import static com.zonzonzon.criteria_wrapper.Calculation.COUNT_DISTINCT;
import static com.zonzonzon.criteria_wrapper.Calculation.MAX;
import static com.zonzonzon.criteria_wrapper.Calculation.MIN;
import static com.zonzonzon.criteria_wrapper.Calculation.SUM;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.function.Consumer;
import javax.persistence.EntityManager;
import javax.persistence.criteria.Predicate;
import javax.persistence.metamodel.SingularAttribute;
import lombok.extern.slf4j.Slf4j;

/**
 * A Facade wrapper to simplify Criteria API usage. It is expected that one instance of this class
 * is used for one query. Query configurations are accumulated and reset after the query is applied.
 *
 * @author Andrey Zonin - Open Source Library.
 */
@Slf4j
public class CriteriaRepository<T> {

  private final EntityManager entityManager;
  private final Class<T> type;
  private final Criteria<T> criteria;

  public CriteriaRepository(EntityManager entityManager, Class<T> type) {
    this.entityManager = entityManager;
    this.type = type;
    this.criteria = new Criteria<>(this.entityManager, this.type);
  }

  /** {@link Common#runInTransaction(Consumer, List)} */
  public void runInTransaction(Consumer<List<?>> consumer, List<?> parameters) {
    criteria.runInTransaction(consumer, parameters);
  }

  /**
   * {@link Create#createInTransaction(Object)}
   *
   * @return Created entity.
   */
  public T createInTransaction(T entity) {
    criteria.createInTransaction(entity);
    return criteria.getSingleResult();
  }

  /** {@link Update#updateInTransaction(Object)} */
  public void updateInTransaction(T entity) {
    criteria.updateInTransaction(entity);
  }

  /** {@link Common#resetQuery()} */
  public void resetQuery() {
    criteria.resetQuery();
  }

  // READ

  /** {@link Read#select(SingularAttribute[])} */
  public Criteria<T> select(SingularAttribute<T, ?>... fields) {
    return criteria.select(fields);
  }

  /** {@link Read#selectAll()} */
  public Criteria<T> selectAll() {
    return criteria.selectAll();
  }

  /** {@link Read#where(Predicate...)} */
  public Criteria<T> where(Predicate... conditions) {
    return criteria.where(conditions);
  }

  /** {@link Result#getSingleResult()} */
  public T getSingleResult() {
    return criteria.getSingleResult();
  }

  /** {@link Result#getResultList(Boolean, List)} */
  public List<T> getResultList(Boolean cached, List<String> embedded) {
    return criteria.getResultList(cached, embedded);
  }

  /**
   * SQL-like SUM() expression.
   *
   * @param entityFieldPath A fieldName or subEntity.fieldName
   * @return Sum of Integer type for the field.
   */
  public Integer sumIntegers(String entityFieldPath) {
    return criteria.getIntegerResult(SUM, entityFieldPath);
  }

  /** An alias for {@link CriteriaRepository#sumIntegers(String)}. */
  public Double sumDoubles(String entityFieldPath) {
    return criteria.getDoubleResult(SUM, entityFieldPath);
  }

  /** An alias for {@link CriteriaRepository#sumIntegers(String)}. */
  public Long sumLongs(String entityFieldPath) {
    return criteria.getLongResult(SUM, entityFieldPath);
  }

  /**
   * SQL-like AVG() expression.
   *
   * @param entityFieldPath A fieldName or subEntity.fieldName
   * @return Average Double value for the field.
   */
  public Double average(String entityFieldPath) {
    return criteria.getDoubleResult(AVERAGE, entityFieldPath);
  }

  /**
   * SQL-like MAX() expression.
   *
   * @param entityFieldPath A fieldName or subEntity.fieldName
   * @return Max Integer value for the field.
   */
  public Integer maxInteger(String entityFieldPath) {
    return criteria.getIntegerResult(MAX, entityFieldPath);
  }

  /** An alias for {@link CriteriaRepository#maxInteger(String)} */
  public Double maxDouble(String entityFieldPath) {
    return criteria.getDoubleResult(MAX, entityFieldPath);
  }

  /** An alias for {@link CriteriaRepository#maxInteger(String)} */
  public Long maxLong(String entityFieldPath) {
    return criteria.getLongResult(MAX, entityFieldPath);
  }

  /** An alias for {@link CriteriaRepository#maxInteger(String)} */
  public ZonedDateTime maxDateTime(String entityFieldPath) {
    return criteria.getZonedDateTimeResult(MAX, entityFieldPath);
  }

  /** An alias for {@link CriteriaRepository#maxInteger(String)} */
  public String maxString(String entityFieldPath) {
    return criteria.getStringResult(MAX, entityFieldPath);
  }

  /**
   * SQL-like MIN() expression.
   *
   * @param entityFieldPath A fieldName or subEntity.fieldName
   * @return Max Integer value for the field.
   */
  public Integer minInteger(String entityFieldPath) {
    return criteria.getIntegerResult(MIN, entityFieldPath);
  }

  /** An alias for {@link CriteriaRepository#minInteger(String)} */
  public Double minDouble(String entityFieldPath) {
    return criteria.getDoubleResult(MIN, entityFieldPath);
  }

  /** An alias for {@link CriteriaRepository#minInteger(String)} */
  public Long minLong(String entityFieldPath) {
    return criteria.getLongResult(MIN, entityFieldPath);
  }

  /** An alias for {@link CriteriaRepository#minInteger(String)} */
  public ZonedDateTime minDateTime(String entityFieldPath) {
    return criteria.getZonedDateTimeResult(MIN, entityFieldPath);
  }

  /** An alias for {@link CriteriaRepository#minInteger(String)} */
  public String minString(String entityFieldPath) {
    return criteria.getStringResult(MIN, entityFieldPath);
  }

  /**
   * SQL-like COUNT() expression.
   *
   * @param entityFieldPath A fieldName or subEntity.fieldName
   * @return Amount of values for the field.
   */
  public Long count(String entityFieldPath) {
    return criteria.getLongResult(COUNT, entityFieldPath);
  }

  /**
   * SQL-like COUNT(DISTINCT *) expression.
   *
   * @param entityFieldPath A fieldName or subEntity.fieldName
   * @return Amount of distinct values for the field.
   */
  public Long countDistinct(String entityFieldPath) {
    return criteria.getLongResult(COUNT_DISTINCT, entityFieldPath);
  }

  // UPDATE

  /** {@link Update#incrementByInteger(String, Integer)} */
  <F> void increment(String fieldPath, F value) {
    criteria.increment(fieldPath, value);
  }

  // FILTER

  /** {@link Filter#inStrings(List, String)} */
  public Predicate inStrings(List<String> value, String nestedObjectValue) {
    return criteria.inStrings(value, nestedObjectValue);
  }

  /** {@link Filter#greaterOrEquals(Object, String)} */
  public <F> Predicate greaterOrEquals(F value, String nestedObjectValue) {
    return criteria.greaterOrEquals(value, nestedObjectValue);
  }

  /** {@link Filter#lessOrEquals(Object, String)} */
  public <F> Predicate lessOrEquals(F value, String nestedObjectValue) {
    return criteria.lessOrEquals(value, nestedObjectValue);
  }

  /** {@link Filter#equals(Object, String)} */
  public <F> Predicate equals(F value, String nestedObjectValue) {
    return criteria.equals(value, nestedObjectValue);
  }

  /** {@link Filter#equals(Object, String)} */
  public <F> Predicate notEquals(F value, String nestedObjectValue) {
    return criteria.notEquals(value, nestedObjectValue);
  }

  /**
   * SQL-like ALL() or ANY() expression.
   *
   * @param leftEntityFieldPath A fieldName or subEntity.fieldName
   * @param compared A comparison operation type.
   * @param rightEntityFieldPath A fieldName or subEntity.fieldName
   * @param <R> A class type of the second entity used to filter out entities of the first type.
   * @return A list of filtered entities of first type.
   */
  public <R> List<T> comparedToInteger(
      String leftEntityFieldPath,
      Calculation calculation,
      It compared,
      Class<R> rightEntityType,
      String rightEntityFieldPath,
      Predicate... conditions) {

    return criteria.getIntegerComparedResults(
        leftEntityFieldPath,
        calculation,
        compared,
        rightEntityType,
        rightEntityFieldPath,
        conditions);
  }

  /**
   * SQL-like ALL() or ANY() expression.
   *
   * @param leftEntityFieldPath A fieldName or subEntity.fieldName
   * @param compared A comparison operation type.
   * @param rightEntityFieldPath A fieldName or subEntity.fieldName
   * @param <R> A class type of the second entity used to filter out entities of the first type.
   * @return A list of filtered entities of first type.
   */
  public <R> List<T> comparedToUuid(
      String leftEntityFieldPath,
      Calculation calculation,
      It compared,
      Class<R> rightEntityType,
      String rightEntityFieldPath,
      Predicate... conditions) {

    // Integer methods fits this case fine:
    return criteria.getIntegerComparedResults(
        leftEntityFieldPath,
        calculation,
        compared,
        rightEntityType,
        rightEntityFieldPath,
        conditions);
  }
}
