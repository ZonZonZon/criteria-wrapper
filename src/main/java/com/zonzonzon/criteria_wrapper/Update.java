package com.zonzonzon.criteria_wrapper;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;
import javax.persistence.criteria.CollectionJoin;
import javax.persistence.criteria.CriteriaBuilder.Case;
import javax.persistence.criteria.CriteriaBuilder.Coalesce;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaBuilder.SimpleCase;
import javax.persistence.criteria.CriteriaBuilder.Trimspec;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.MapJoin;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;
import javax.persistence.criteria.Subquery;
import javax.persistence.metamodel.SingularAttribute;

/**
 * Methods providing update operations.
 *
 * @param <T> Type of entity for queries.
 */
public interface Update<T> extends Common<T> {

  /**
   * Update an entity.
   *
   * @param entity Entity instance.
   */
  default void update(T entity) {
    getEntityManager().persist(entity);
    resetQuery();
  }

  /**
   * Update an entity in an isolated transaction.
   *
   * @param entity Entity instance.
   */
  default void updateInTransaction(T entity) {
    runInTransaction(parameters -> update(entity), List.of(entity));
  }

  /**
   * Update all records by some condition.
   *
   * @param fieldToUpdate A field to update.
   * @param operation An operation to apply for update.
   * @param clauses A condition to filter rows for update. May be NULL.
   * @param <F> A type of updated field.
   */
  default <F> void updateMany(
      SingularAttribute<T, F> fieldToUpdate, Expression<F> operation, List<Predicate> clauses) {

    CriteriaUpdate<T> criteriaUpdate = getBuilder().createCriteriaUpdate(getType());
    criteriaUpdate.from(getType());
    criteriaUpdate.set(fieldToUpdate, operation);
    if (clauses != null && !clauses.isEmpty()) {
      criteriaUpdate.where(clauses.toArray(new Predicate[0]));
    }
    getEntityManager().createQuery(criteriaUpdate).executeUpdate();
    resetQuery();
  }

  /**
   * Update all records by some condition.
   *
   * @param fieldToUpdate A field to update.
   * @param value A value to apply for update.
   * @param clauses A condition to filter rows for update. May be NULL.
   * @param <F> A type of updated field.
   */
  default <F> void updateMany(
      SingularAttribute<T, F> fieldToUpdate, F value, Collection<Predicate> clauses) {

    CriteriaUpdate<T> updateQuery = getBuilder().createCriteriaUpdate(getType());
    updateQuery.from(getType());
    updateQuery.set(fieldToUpdate, value);
    if (clauses != null && !clauses.isEmpty()) {
      updateQuery.where(clauses.toArray(new Predicate[0]));
    }
    getEntityManager().createQuery(updateQuery).executeUpdate();
    resetQuery();
  }

  default <F> void increment(String fieldPath, F value) {
    if (value instanceof Integer) {
      incrementByInteger(fieldPath, (Integer) value);
    }
    if (value instanceof Long) {
      incrementByLong(fieldPath, (Long) value);
    }
    if (value instanceof Double) {
      incrementByDouble(fieldPath, (Double) value);
    }
  }

  default void incrementByInteger(String fieldPath, Integer value) {
    CriteriaUpdate<T> updateQuery = getCriteria().getUpdateQuery();
    Root<T> root = updateQuery.from(getType());
    Path<Integer> path = getPath(fieldPath, root);
    Expression<Integer> expression = getBuilder().sum(path, value);
    updateQuery.set(path, expression);
    Query query = getEntityManager().createQuery(updateQuery);
    int rowsUpdated = query.executeUpdate();
    getLog().info(String.format("Updated %s rows", rowsUpdated));
  }

  default void incrementByLong(String fieldPath, Long value) {
    CriteriaUpdate<T> updateQuery = getCriteria().getUpdateQuery();
    Root<T> root = updateQuery.from(getType());
    Path<Long> path = getPath(fieldPath, root);
    Expression<Long> expression = getBuilder().sum(path, value);
    updateQuery.set(path, expression);
    Query query = getEntityManager().createQuery(updateQuery);
    long rowsUpdated = query.executeUpdate();
    getLog().info(String.format("Updated %s rows", rowsUpdated));
  }

  default void incrementByDouble(String fieldPath, Double value) {
    CriteriaUpdate<T> updateQuery = getCriteria().getUpdateQuery();
    Root<T> root = updateQuery.from(getType());
    Path<Double> path = getPath(fieldPath, root);
    Expression<Double> expression = getBuilder().sum(path, value);
    updateQuery.set(path, expression);
    Query query = getEntityManager().createQuery(updateQuery);
    double rowsUpdated = query.executeUpdate();
    getLog().info(String.format("Updated %s rows", rowsUpdated));
  }

  default <N extends Number> Expression<N> neg(Expression<N> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <N extends Number> Expression<N> abs(Expression<N> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <N extends Number> Expression<N> prod(
      Expression<? extends N> x, Expression<? extends N> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <N extends Number> Expression<N> prod(Expression<? extends N> x, N y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <N extends Number> Expression<N> prod(N x, Expression<? extends N> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <N extends Number> Expression<N> diff(
      Expression<? extends N> x, Expression<? extends N> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <N extends Number> Expression<N> diff(Expression<? extends N> x, N y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <N extends Number> Expression<N> diff(N x, Expression<? extends N> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Number> quot(Expression<? extends Number> x, Expression<? extends Number> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Number> quot(Expression<? extends Number> x, Number y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Number> quot(Number x, Expression<? extends Number> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Integer> mod(Expression<Integer> x, Expression<Integer> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Integer> mod(Expression<Integer> x, Integer y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Integer> mod(Integer x, Expression<Integer> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Double> sqrt(Expression<? extends Number> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Long> toLong(Expression<? extends Number> number) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Integer> toInteger(Expression<? extends Number> number) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Float> toFloat(Expression<? extends Number> number) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Double> toDouble(Expression<? extends Number> number) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<BigDecimal> toBigDecimal(Expression<? extends Number> number) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<BigInteger> toBigInteger(Expression<? extends Number> number) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> toString(Expression<Character> character) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <T> Expression<T> literal(T value) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <T> Expression<T> nullLiteral(Class<T> resultClass) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> concat(Expression<String> x, Expression<String> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> concat(Expression<String> x, String y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> concat(String x, Expression<String> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> substring(Expression<String> x, Expression<Integer> from) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> substring(Expression<String> x, int from) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> substring(
      Expression<String> x, Expression<Integer> from, Expression<Integer> len) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> substring(Expression<String> x, int from, int len) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> trim(Expression<String> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> trim(Trimspec ts, Expression<String> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> trim(Expression<Character> t, Expression<String> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> trim(Trimspec ts, Expression<Character> t, Expression<String> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> trim(char t, Expression<String> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> trim(Trimspec ts, char t, Expression<String> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> lower(Expression<String> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<String> upper(Expression<String> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Integer> length(Expression<String> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Integer> locate(Expression<String> x, Expression<String> pattern) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Integer> locate(Expression<String> x, String pattern) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Integer> locate(
      Expression<String> x, Expression<String> pattern, Expression<Integer> from) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Integer> locate(Expression<String> x, String pattern, int from) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Date> currentDate() {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Timestamp> currentTimestamp() {
    getLog().error("Not implemented yet");
    return null;
  }

  default Expression<Time> currentTime() {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y> Expression<Y> coalesce(Expression<? extends Y> x, Expression<? extends Y> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y> Expression<Y> coalesce(Expression<? extends Y> x, Y y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y> Expression<Y> nullif(Expression<Y> x, Expression<?> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y> Expression<Y> nullif(Expression<Y> x, Y y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <T> Expression<T> function(String name, Class<T> type, Expression<?>... args) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <T> ParameterExpression<T> parameter(Class<T> paramClass) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <T> ParameterExpression<T> parameter(Class<T> paramClass, String name) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate exists(Subquery<?> subquery) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate and(Expression<Boolean> x, Expression<Boolean> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate and(Predicate... restrictions) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate or(Expression<Boolean> x, Expression<Boolean> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate or(Predicate... restrictions) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate not(Expression<Boolean> restriction) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate conjunction() {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate disjunction() {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate isTrue(Expression<Boolean> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate isFalse(Expression<Boolean> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate isNull(Expression<?> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate isNotNull(Expression<?> x) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate equal(Expression<?> x, Expression<?> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate equal(Expression<?> x, Object y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate notEqual(Expression<?> x, Expression<?> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate notEqual(Expression<?> x, Object y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y extends Comparable<? super Y>> Predicate greaterThan(
      Expression<? extends Y> x, Expression<? extends Y> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y extends Comparable<? super Y>> Predicate greaterThan(Expression<? extends Y> x, Y y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y extends Comparable<? super Y>> Predicate greaterThanOrEqualTo(
      Expression<? extends Y> x, Expression<? extends Y> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y extends Comparable<? super Y>> Predicate greaterThanOrEqualTo(
      Expression<? extends Y> x, Y y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y extends Comparable<? super Y>> Predicate lessThan(
      Expression<? extends Y> x, Expression<? extends Y> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y extends Comparable<? super Y>> Predicate lessThan(Expression<? extends Y> x, Y y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y extends Comparable<? super Y>> Predicate lessThanOrEqualTo(
      Expression<? extends Y> x, Expression<? extends Y> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y extends Comparable<? super Y>> Predicate lessThanOrEqualTo(
      Expression<? extends Y> x, Y y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y extends Comparable<? super Y>> Predicate between(
      Expression<? extends Y> v, Expression<? extends Y> x, Expression<? extends Y> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <Y extends Comparable<? super Y>> Predicate between(Expression<? extends Y> v, Y x, Y y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate gt(Expression<? extends Number> x, Expression<? extends Number> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate gt(Expression<? extends Number> x, Number y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate ge(Expression<? extends Number> x, Expression<? extends Number> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate ge(Expression<? extends Number> x, Number y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate lt(Expression<? extends Number> x, Expression<? extends Number> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate lt(Expression<? extends Number> x, Number y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate le(Expression<? extends Number> x, Expression<? extends Number> y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate le(Expression<? extends Number> x, Number y) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <C extends Collection<?>> Predicate isEmpty(Expression<C> collection) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <C extends Collection<?>> Predicate isNotEmpty(Expression<C> collection) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <E, C extends Collection<E>> Predicate isMember(
      Expression<E> elem, Expression<C> collection) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <E, C extends Collection<E>> Predicate isMember(E elem, Expression<C> collection) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <E, C extends Collection<E>> Predicate isNotMember(
      Expression<E> elem, Expression<C> collection) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <E, C extends Collection<E>> Predicate isNotMember(E elem, Expression<C> collection) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate like(Expression<String> x, Expression<String> pattern) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate like(Expression<String> x, String pattern) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate like(
      Expression<String> x, Expression<String> pattern, Expression<Character> escapeChar) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate like(Expression<String> x, Expression<String> pattern, char escapeChar) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate like(Expression<String> x, String pattern, Expression<Character> escapeChar) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate like(Expression<String> x, String pattern, char escapeChar) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate notLike(Expression<String> x, Expression<String> pattern) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate notLike(Expression<String> x, String pattern) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate notLike(
      Expression<String> x, Expression<String> pattern, Expression<Character> escapeChar) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate notLike(Expression<String> x, Expression<String> pattern, char escapeChar) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate notLike(
      Expression<String> x, String pattern, Expression<Character> escapeChar) {
    getLog().error("Not implemented yet");
    return null;
  }

  default Predicate notLike(Expression<String> x, String pattern, char escapeChar) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <T> Coalesce<T> coalesce() {
    getLog().error("Not implemented yet");
    return null;
  }

  default <C, R> SimpleCase<C, R> selectCase(Expression<? extends C> expression) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <R> Case<R> selectCase() {
    getLog().error("Not implemented yet");
    return null;
  }

  default <C extends Collection<?>> Expression<Integer> size(Expression<C> collection) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <C extends Collection<?>> Expression<Integer> size(C collection) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <V, M extends Map<?, V>> Expression<Collection<V>> values(M map) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <K, M extends Map<K, ?>> Expression<Set<K>> keys(M map) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <T> In<T> in(Expression<? extends T> expression) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <X, T, V extends T> Join<X, V> treat(Join<X, T> join, Class<V> type) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <X, T, E extends T> CollectionJoin<X, E> treat(CollectionJoin<X, T> join, Class<E> type) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <X, T, E extends T> SetJoin<X, E> treat(SetJoin<X, T> join, Class<E> type) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <X, T, E extends T> ListJoin<X, E> treat(ListJoin<X, T> join, Class<E> type) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <X, K, T, V extends T> MapJoin<X, K, V> treat(MapJoin<X, K, T> join, Class<V> type) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <X, T extends X> Path<T> treat(Path<X> path, Class<T> type) {
    getLog().error("Not implemented yet");
    return null;
  }

  default <X, T extends X> Root<T> treat(Root<X> root, Class<T> type) {
    getLog().error("Not implemented yet");
    return null;
  }
}
