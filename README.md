# Criteria Wrapper

A library to simplify work with <code>javax.persistence.criteriaRepository</code> library. Covers most of the common use cases. 

Major advantage is an ability to build dynamic fetch graphs and pass paths to sub-entities as dot-chained strings. In case of Typed MetaModel Generation, you can use an interlayer to map POJOs to chained strings.  

For uncovered cases feel free to use standard Criteria API.

## Technologies

- JPA
- Spring Data
- Lombok
- Jupiter
- TestContainers

This library is purely JPA based. Along with it, consider adding suitable JPA MetaModel implementation in you project.

## Functionality

- CRUD
- Sorting & Pagination
- Dynamic fetch graphs built from "embedded" string (dot-separated) to define the scope of nested
  sub-entities to be loaded
- Caching queries

## Integration

Add to your <code>pom.xml</code>:

    <dependency>
      <groupId>com.zonzonzon</groupId>
      <artifactId>criteriaRepository-wrapper</artifactId>
      <version>1.0</version>
    </dependency>

## Criteria Repository

Create one Criteria repository per entity:

    CriteriaRepository<MyEntity> repository = new CriteriaRepository<>(entityManager, MyEntity.class);

## Create Examples:

Persist an entity in an isolated transaction and read it the next line:

    MyEntity myEntity = MyEntity.builder().build();
    MyEntity createdEntity = repository.createInTransaction(myEntity);

## Update Examples:

Simple persistence of entity changes:
    
    repository.updateInTransaction(myChangedEntity);

Increment by a value:

    repository.increment("value", 10)
    repository.increment("money", 10D)
    repository.increment("starsCount", 10L)

## Delete Examples:



## Read Examples:

Query setting (filtering, sorting, pagination) are reset to default after each request or manually:
 
    repository.resetQuery();

Read with filtering:

    Predicate whereClause = repository.inStrings(List.of("Mike", "John"), "name");
    boolean isCached = false;
    List<MyEntity> results = repository.select().where(whereClause).getResultList(isCached, emptyList(););

Calculate (sum, average, max, min, count, countDistinct) from a field of type (Integer, Double, Long, String, ZonedDateTime):

    int result = repository.sumIntegers("mySubEntity.mySubSubEntity.value");

Filter selection comparing (all, any) by fields of another entity:

    Predicate greaterOrEquals = repository.greaterOrEquals(555, "value");

    results =
        repository.comparedToInteger(
            "value", Calculation.ALL, It.GREATER_THAN, MySubEntity.class, "value", greaterOrEquals);




